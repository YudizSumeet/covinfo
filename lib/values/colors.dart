import 'package:flutter/material.dart';

/// A class containing colors used in the app

class AppColors {
  static const Color colorPrimary = Color(0xff0D0E58);

  static const Color app_black = Color(0xff212121);
  static const Color app_white = Color(0xffffffff);

  static const Color grey_hint = Color(0xff9e9e9e);
  static const Color grey_light = Color(0xffe2e2ea);

//  static const Color red_light = Color(0xffF0A7A4);
//  static const Color blue_light = Color(0xff4AA6E7);
//  static const Color red_dark = Color(0xffEE3D3D);

  static const Color red_light = Color(0xffFF9900);
  static const Color blue_light = Color(0xff00B62F);
  static const Color red_dark = Color(0xffF13B36);

  static const Color yellow_dark = Color(0xffFF7977);
//  static const Color textColorOrange = Color(0xffF0A7A4);
//  static const Color textColorDarkBlue = Color(0xff0D0E58);
  static const Color textColorDarkBlue = Color(0xff0D0E58);

  static const Color light_grayish = Color(0xffDEDEE7);
//  static const Color barColorLightPink = Color(0xffF2A7A3);
}