import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:corona_yudiz/values/l10n/messages_all.dart';

class AppLocalizations {
  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return AppLocalizations();
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  String get welcome {
    return Intl.message('Welcome', name: 'welcome');
  }

  String get e_msg_something_went_worng {
    return Intl.message('Something want worng', name: 'e_msg_something_went_worng');
  }

  String get t_covid_19 {
    return Intl.message('COVID 19', name: 't_covid_19');
  }

  String get total_confirm_cases {
    return Intl.message('Total Confirmed Cases', name: 'total_confirm_cases');
  }

  String get total_recovered {
    return Intl.message('Total Recovered', name: 'total_recovered');
  }

  String get total_death {
    return Intl.message('Total Death', name: 'total_death');
  }

  String get number_of_cases {
    return Intl.message('Number Of Cases', name: 'number_of_cases');
  }

  String get critical_vs_mild {
    return Intl.message('Critical vs Mild', name: 'critical_vs_mild');
  }

  String get confirmed {
    return Intl.message('Confirmed', name: 'confirmed');
  }

  String get confirm_cases {
    return Intl.message('Confirmed Cases', name: 'confirm_cases');
  }

  String get recovered {
    return Intl.message('Recovered', name: 'recovered');
  }

  String get death {
    return Intl.message('Death', name: 'death');
  }

  String get corona_symptoms {
    return Intl.message('Corona Symptoms', name: 'corona_symptoms');
  }

  String get corona_precautions {
    return Intl.message('Corona Precautions', name: 'corona_precautions');
  }

  String get emergency_numbers {
    return Intl.message('Emergency Numbers', name: 'emergency_numbers');
  }

  String get about_us {
    return Intl.message('About Us', name: 'about_us');
  }

  String get contact_us {
    return Intl.message('Contact Us', name: 'contact_us');
  }

  String get settings {
    return Intl.message('Change Country', name: 'settings');
  }

  String get share_app {
    return Intl.message('Share app', name: 'share_app');
  }

  String get privacy_policy {
    return Intl.message('Privacy Policy', name: 'privacy_policy');
  }

  String get select_country {
    return Intl.message('Select Country', name: 'select_country');
  }

  String get search_country_data {
    return Intl.message('Search Country Data', name: 'search_country_data');
  }

  String get all_country_data {
    return Intl.message('All Country Data', name: 'all_country_data');
  }

  String get country {
    return Intl.message('Country', name: 'country');
  }

  String get total_cases {
    return Intl.message('Total', name: 'total_cases');
  }

  String get new_cases {
    return Intl.message('New Cases', name: 'new_cases');
  }

  String get active_cases {
    return Intl.message('Active', name: 'active_cases');
  }

  String get deaths {
    return Intl.message('Deaths', name: 'deaths');
  }

  String get no_data_found {
    return Intl.message('No data found', name: 'no_data_found');
  }

  String get all_india_data {
    return Intl.message('All India Data', name: 'all_india_data');
  }

  String get last_updated {
    return Intl.message('Last Updated :', name: 'last_updated');
  }

  String get total_cases_in_india {
    return Intl.message('Total Cases In India', name: 'total_cases_in_india');
  }

  String get state_ut {
    return Intl.message('STATE/UT', name: 'state_ut');
  }

  String get active {
    return Intl.message('ACTIVE', name: 'active');
  }

}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'ar'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }
}
