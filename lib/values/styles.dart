import 'package:corona_yudiz/constants.dart';
import 'package:flutter/material.dart';

import 'package:corona_yudiz/values/colors.dart';

/// A class containing common styles of the widgets like texts, text fields

class Styles {
  static TextStyle tvStyle(
      {fontFamily = AppConstants.fontAvenir,
      fontSize = 16.0,
      fontWeight = FontWeight.w400,
      textColor = AppColors.colorPrimary,
      fontStyle = FontStyle.normal,
      textDecoration}) {
    return TextStyle(
        fontStyle: fontStyle,
        decoration: textDecoration,
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: textColor);
  }

  static TextStyle hintStyle({
    fontFamily = AppConstants.fontAvenir,
    fontWeight = FontWeight.w400,
    fontSize = 16.0,
    textColor = AppColors.grey_hint,
  }) {
    return TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: textColor);
  }

  static InputDecoration etDecoration(
      {@required hintText,
      @required border,
      isError = false,
      onIconPressed,
      @required hintStyle,
      textColor = AppColors.app_black}) {
    return InputDecoration(
        hasFloatingPlaceholder: true,
        contentPadding: const EdgeInsets.all(5),
        border: border,
        suffixIcon: isError
            ? IconButton(
                onPressed: onIconPressed,
                icon: Icon(Icons.close),
                color: Colors.redAccent,
              )
            : null,
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: AppColors.grey_hint)),
        labelStyle: hintStyle,
        labelText: hintText);
  }
}
