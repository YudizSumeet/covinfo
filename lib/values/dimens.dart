import 'dart:ui';

import 'package:flutter/material.dart';

/// A class containing common dimensions used in the app

class Dimens {
  static var horizontalSizeMultiplier; //image, width
  static var verticalSizeMultiplier; //text, height
  double headingTextSize;
  double arrowImageSize;
  double w190;
  double t7, t9, t10, t12, t13, t15;

  Dimens._(); //to disable default constructor

  Dimens({Size size, orientation = Orientation.portrait}) {
    if (orientation == Orientation.portrait) {
      horizontalSizeMultiplier = size.width / 100;
      verticalSizeMultiplier = size.height / 100;
    } else {
      verticalSizeMultiplier = size.width / 100;
      horizontalSizeMultiplier = size.height / 100;
    }

    //mi note 3 => vertic = 6.4, horiz = 3.6

    headingTextSize  = 2 * verticalSizeMultiplier; //20, given verticalSizeMultiplier is 10
    headingTextSize  = 3 * horizontalSizeMultiplier; //45, given horizontalSizeMultiplier is 15
    w190  = 52.7778 * horizontalSizeMultiplier; //45, given horizontalSizeMultiplier is 15
    t7  = 1.09 * verticalSizeMultiplier;
    t9  = 1.406 * verticalSizeMultiplier;
    t10  = 1.5625 * verticalSizeMultiplier;
    t12  = 1.875 * verticalSizeMultiplier;
    t13  = 2.031 * verticalSizeMultiplier;
    t15  = 2.5 * verticalSizeMultiplier;
  }

//  final double pagePadding = 20.0;
//  final double toolbarElevation = 2.0;
}
