import 'package:flutter/material.dart';

class AppNotifier with ChangeNotifier {
  Locale _currentLocale = Locale('en');

  Locale get currentLocale => _currentLocale;

  set currentLocale(Locale value) {
    _currentLocale = value;
  }

  void changeLocale(Locale newLocale) {
    _currentLocale = newLocale;
    notifyListeners();
  }
}