/// A class containing constants used in the app including API endpoints, Date formats

class AppConstants {
  static const String localImageBasePath = "res/drawable/";
  static const String fontAvenir = "Avenir";
  static const String fontRoboto = "roboto";
  static const String REQ_DATE_FORMAT = "dd/MM/yyyy hh:mm:ss";
  static const String EXTRA_SELECTED_COUNTRY = "extra_selected_country";
  static const String NUMBER_FORMAT = "#,###,###";
  static const String DATE_FORMAT = "MMM dd";
  static const String ALL_COUNTRY = "All Country";
  static const String KEY_GLOBAL = "Global";
  static const String KEY_SEL_COUNTRY = "selCountry";
  static const String KEY_CORONA_LEVEL = "coronaLevelPopup";
  static const String INDIA_COUNTRY_CODE = "IN";

  static const String COUNTRY_LIST_JSON_FILE_PATH = "assets/country-codes-lat-long-alpha3.json";

  static const String SHARE_APP_SUBJECT = """Download app for Covid19 update and Spread awareness 

  https://www.covinfo.org

  Volunteer driven initiative building application for tracking covid19 patients using API from covid19india for India’s data and API from worldometers for world data.

  Features

  1. Keep up with live numbers.
  Compact dashboard with realtime information.

  2. Visualize the spreading.
  Graphical representation of daily statistics.

  3. Keep an eye on the world.
  Traverse global data with an interactive map.

  4. Refine the statistics.
  Countrywise tabular data with filters.""";
  static const String SHARE_APP_MESSAGE = "Download the app from the link given below to check the statistics around the world, as well as state-wise and to learn more about the corona. www.covinfo.org";
}

class ApiConfig {
  static const BASE_URL = "https://gitlab.com/YudizSumeet/corono-project-data/-/raw/master/"; //live
  static const PRIVACY_POLICY_URL = "https://www.covinfo.org/privacy-policy.html";
}

class APIEndPoint {
  static const LOGIN = "/login";
  static const SOURCES = "https://gitlab.com/YudizSumeet/corono-project-data/-/raw/master/references.json";
  static const MORE = "https://gitlab.com/YudizSumeet/corono-project-data/-/raw/master/more.json";
}

class APIRequestCode {
  static const int LOGIN = 1;
}

class BundleKey {
  static const int PROFILE_DATA = 1;
}
