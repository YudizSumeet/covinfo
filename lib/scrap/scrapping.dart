import 'package:http/http.dart' as http;

class Scrapping {
  var url = 'https://www.worldometers.info/coronavirus/';

  //list => total cases, new cases, total death, new death, total recovered, active cases, serious

  Future<Map<String, List<dynamic>>> getAllData() async {
    var countryData = {
      "Global": [0, 0, 0, 0, 0, 0, 0, 0.0, true]
    };

    var response = await http.get(url);
    if (response.statusCode == 200) {
      var row = response.body
          .split("<tr class=\"total_row\">")[1]
          .split("</tr>")[0]
          .split(">");

      countryData["Global"] = parseRow(row, true, "");

      print(countryData);

      var tbody = getInnerString(response.body, "<tbody>", "</tbody>");
      var rows = tbody.split("<tr style=\"\">");
      rows.skip(1).forEach((rawRow) {
        row = rawRow.split(">");
        bool hasInnerTag =
            rawRow.contains("</a>") || rawRow.contains("</span>");
        countryData[normalizeName(row[hasInnerTag ? 2 : 1].split("<")[0])] =
            parseRow(
                row,
                hasInnerTag,
                rawRow.contains("</a>")
                    ? getInnerString(rawRow, "href=\"", "\"")
                    : null);
      });
    }
    return countryData;
  }

  Future<Map<List, List>> getCountryData(
      Map countryData, String country) async {
    var response;

    print("countryData $countryData");

    if (country != "Global") {
      url += countryData[country][8];
      response = await http.get(url);
      if (response.statusCode != 200) return Map();
    }

    var xLabels = response.body
        .split("categories: [")[4]
        .split("]")[0]
        .replaceAll("\"", "")
        .split(",");
    var values = response.body
        .split("data: [")[4]
        .split("]")[0]
        .split(",")
        .map(int.parse)
        .toList();

    var xLabels2 = response.body
        .split("categories: [")[1]
        .split("]")[0]
        .replaceAll("\"", "")
        .split(",");
    var values2 = response.body
        .split("data: [")[1]
        .split("]")[0]
        .split(",")
        .map(int.parse)
        .toList();

    var xLabels3 = response.body
        .split("categories: [")[country == "Global" ? 6 : 8]
        .split("]")[0]
        .replaceAll("\"", "")
        .split(",");
    var values3 = response.body
        .split("data: [")[country == "Global" ? 6 : 8]
        .split("]")[0]
        .split(",")
        .map(int.parse)
        .toList();

    values2.asMap().forEach((index, value) {
      values2[index] = values[index] - values3[index] - value;
    });

    return {
      xLabels: [values, values2, values3]
    };

    //total cases, total recovered, total death
  }

  String getInnerString(String source, String a, String b) {
    return source.split(a)[1].split(b)[0];
  }

  String normalizeName(String n) {
    return n.replaceAll("&ccedil;", "ç").replaceAll("&eacute;", "é");
  }

  List parseRow(List<String> row, bool hasInnerTag, String link) {
    int offset = hasInnerTag ? 0 : -2;
    return [
      parseInteger(row[5 + offset]),
      parseInteger(row[7 + offset]),
      parseInteger(row[9 + offset]),
      parseInteger(row[11 + offset]),
      parseInteger(row[13 + offset]),
      parseInteger(row[15 + offset]),
      parseInteger(row[17 + offset]),
      parseDouble(row[19 + offset]),
      link
    ];
  }

  double parseDouble(String s) {
    try {
      return double.parse(
          s.split("<")[0].replaceAll(",", "").replaceAll("+", ""));
    } catch (e) {
      return 0;
    }
  }

  int parseInteger(String s) {
    try {
      return int.parse(s.split("<")[0].replaceAll(",", "").replaceAll("+", ""));
    } catch (e) {
      return 0;
    }
  }
}
