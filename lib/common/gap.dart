import 'package:flutter/material.dart';

/// A widget to add a gap between other widgets

class Gap extends StatelessWidget {
  final double gap;
  final bool isVertical;

  Gap({this.gap = 15.0, this.isVertical = true});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: isVertical ? gap : 0.0,
      width: isVertical ? 0.0 : gap,
    );
  }
}
