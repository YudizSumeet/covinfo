import 'package:flutter/material.dart';
import 'package:corona_yudiz/common/gap.dart';
import 'package:corona_yudiz/custom/shadow_view.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build bottom button as per app's theme

class BottomButton extends StatelessWidget {
  final Function onClick;

  const BottomButton({@required this.onClick});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: ShadowView(
        backgroundColor: AppColors.colorPrimary,
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.add,
              color: Colors.white,
            ),
            Gap(
              isVertical: false,
              gap: 2.0,
            ),
            Text(
              AppLocalizations.of(context).welcome,
              style: Styles.tvStyle(
                  textColor: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0),
            )
          ],
        ),
      ),
    );
  }
}
