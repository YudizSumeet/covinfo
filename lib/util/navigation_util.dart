import 'package:flutter/material.dart';

/// A class to handle screen navigation in the app

class NavigationUtil {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigatePush(Widget widget) {
    return navigatorKey.currentState.push(
      MaterialPageRoute(builder: (context) => widget),
    );
  }

  bool canPop() {
    return navigatorKey.currentState.canPop();
  }

  //returns true if popped
  bool navigatePop({Map result}) {
    if (canPop()) {
      navigatorKey.currentState.pop(result);
      return true;
    }
    return false;
  }

  void navigatePushReplace(Widget widget) {
    navigatorKey.currentState
        .pushReplacement(MaterialPageRoute(builder: (context) => widget));
  }

  void pushReplacementNamed(Widget widget) {
//    navigatorKey.currentState
//        .pushReplacement(MaterialPageRoute(builder: (context) => widget));

    navigatorKey.currentState.pushReplacementNamed('/');
  }
}
