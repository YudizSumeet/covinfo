/// A class containing common validations used in the app

class ValidationUtil {
  static final int passwordMinLength = 6;

  static bool arePasswordsSame(String password1, String password2) {
    return password1 == password2;
  }

  static bool isEmailValid(String email) {
    String emailRegex =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    return RegExp(emailRegex).hasMatch(email);
  }

  static bool isPasswordValid(String password) {
    //Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
    String passwordRegex =
        r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$';
    return RegExp(passwordRegex).hasMatch(password);
  }
}
