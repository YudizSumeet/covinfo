import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:package_info/package_info.dart';

/// A class containing common utilities used in the app

class AppUtil {

  static void getFullScreen() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  static void revertFullScreen() {
    SystemChrome.setEnabledSystemUIOverlays(
        SystemUiOverlay.values); //full screen revert
  }

  static void hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static Size getDeviceSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  static Future<String> getBuildNumber() async {
    var info = await PackageInfo.fromPlatform();
    return info.buildNumber;
  }

  static Future<String> getVersion() async {
    var info = await PackageInfo.fromPlatform();
    return info.version;
  }

  /*By default, Android opens up a browser when handling URLs.
  You can pass forceWebView: true parameter to tell the plugin to open a WebView instead.
  On iOS, the default behavior is to open all web URLs within the app.
  Everything else is redirected to the app handler.*/
  static openBrowser(String url) async {
    if (await canLaunch(url))
      await launch(url);
    else
      throw 'Could not open the browser.';
  }

  static openMail(String to, {String subject = "", String body = ""}) async {
    String mailUrl = "mailto:$to?subject=$subject&body=$body";
    if (await canLaunch(mailUrl))
      await launch(mailUrl);
    else
      throw 'Could not open the mail client.';
  }

  static openPhoneDial(String number) async {
    String phoneUrl = "tel:$number";
    if (await canLaunch(phoneUrl))
      await launch(phoneUrl);
    else
      throw 'Could not open the phone dial';
  }
}

class SpannableTextView extends StatelessWidget {
  final Map<String, bool> stringMap;
  final bool isBold;
  final bool isUnderline;
  final Color colorSpannable, color;
  final Function(String) onClick;
  final double fontSize;
  final TextAlign align;

  SpannableTextView(
      {@required this.stringMap,
      @required this.onClick,
      this.fontSize = 16.0,
      this.align = TextAlign.center,
      this.isBold = false,
      this.color = Colors.black,
      this.colorSpannable = Colors.black,
      this.isUnderline = false});

  @override
  Widget build(BuildContext context) {
    List<TextSpan> textSpanList = List();

    stringMap.forEach((text, isSpannable) {
      textSpanList.add(TextSpan(
        style: Styles.tvStyle(
            textColor: isSpannable ? colorSpannable : color,
            fontSize: fontSize,
            textDecoration:
                isSpannable && isUnderline ? TextDecoration.underline : null,
            fontWeight: isSpannable && isBold ? FontWeight.bold : null),
        text: text,
        recognizer: TapGestureRecognizer()
          ..onTap = () => isSpannable ? onClick(text) : null,
      ));
    });

    return RichText(
      textAlign: align,
      text: TextSpan(children: textSpanList),
    );
  }
}
