import 'package:corona_yudiz/constants.dart';
import 'package:dio/dio.dart';
import 'package:corona_yudiz/api/api_connector.dart';

class ApiUtil {
  ApiConnector _apiConnector;

  ApiUtil() {
    _apiConnector = ApiConnector();
  }

  void resetToken() {
    _apiConnector.resetToken();
  }

  void cancelRequests({CancelToken cancelToken}) {
    _apiConnector.cancelRequests(cancelToken: cancelToken);
  }

  dynamic login() async {
    return (await _apiConnector.get("/todos/1")).data;
  }
  dynamic sources() async {
    return (await _apiConnector.get(APIEndPoint.SOURCES)).data;
  }
}