import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:corona_yudiz/constants.dart';

class ApiConnector {
  Dio _dio;
  String tag = "API call :";
  CancelToken _cancelToken;

  ApiConnector() {
    _dio = createDio();
  }

  Dio createDio() {
    _cancelToken = CancelToken();
    return Dio(
      _getDioConfig(ApiConfig.BASE_URL),
    )..interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        debugPrint("$tag ${options.toString()}", wrapWidth: 1024);
        return options;
      }, onResponse: (Response response) async {
        debugPrint("$tag  ${response.toString()}", wrapWidth: 1024);
        return response;
      }, onError: (DioError e) async {
        debugPrint("$tag ${e.toString()}", wrapWidth: 1024);
        debugPrint("$tag ${e.request.toString()}", wrapWidth: 1024);
        debugPrint("$tag ${e.response.toString()}", wrapWidth: 1024);
        return e;
      }));
  }

  BaseOptions _getDioConfig(String baseUrl) {
    return BaseOptions(
        connectTimeout: 10 * 1000,
        receiveTimeout: 10 * 1000,
        baseUrl: baseUrl,
        contentType: 'application/json',
        headers: {
          'Authorization': "",
        });
  }

  void resetToken() {
    _dio = createDio();
  }

  void cancelRequests({CancelToken cancelToken}) {
    cancelToken == null
        ? _cancelToken.cancel('Cancelled')
        : cancelToken.cancel();

    _cancelToken = CancelToken();
  }

  Future<Response> get(
    String endUrl, {
    Map<String, dynamic> params,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.get(
                endUrl,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).get(
                endUrl,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }

  Future<Response> post(
    String endUrl, {
    Map<String, dynamic> params,
    bool isFormData = true,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.post(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).post(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }

  Future<Response> patch(
    String endUrl, {
    Map<String, dynamic> params,
    bool isFormData = true,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.patch(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).patch(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }

  Future<Response> delete(
    String endUrl, {
    Map<String, dynamic> params,
    bool isFormData = true,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.delete(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).delete(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }

  Future<Response> put(
    String endUrl, {
    Map<String, dynamic> params,
    bool isFormData = true,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.put(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).put(
                endUrl,
                data: isFormData ? FormData.fromMap(params) : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }

  Future<Response> multiPart(
    String endUrl, {
    Map<String, dynamic> params,
    MapEntry<String, String> file, //file (param name -> path)
    bool isFormData = true,
    Options options,
    String baseUrl,
    CancelToken cancelToken,
  }) async {
    return (await (baseUrl == null
            ? _dio.post(
                endUrl,
                data: isFormData
                    ? FormData.fromMap(params
                      ..addAll({
                        file.key: await MultipartFile.fromFile(file.value),
                      }))
                    : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              )
            : Dio(_getDioConfig(baseUrl)).post(
                endUrl,
                data: isFormData
                    ? FormData.fromMap(params
                      ..addAll({
                        file.key: await MultipartFile.fromFile(file.value),
                      }))
                    : null,
                queryParameters: params,
                cancelToken: cancelToken ?? _cancelToken,
                options: options,
              ))
        .catchError((e) {
      throw e;
    }));
  }
}
