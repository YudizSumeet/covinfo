import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:loading_indicator/loading_indicator.dart';

/// A widget to show the progress when the API is being called

class Progress {
  static OverlayEntry overlayEntry;
  static OverlayState overlayState;
  static bool _isProgressShown = false;

  static void showProgress(BuildContext context) {
    AppUtil.hideKeyboard(context);

    if(!_isProgressShown) {
      overlayState = Overlay.of(context);
      overlayState.insert(getOverlayEntry());
      _isProgressShown = true;
    }
  }

  static OverlayEntry getOverlayEntry() {
    if (overlayEntry == null)
      overlayEntry = OverlayEntry(
          builder: (context) =>
              Container(color: Colors.transparent, child: ProgressWidget()));
    return overlayEntry;
  }

  static void hideProgress() {
    if (_isProgressShown) {
      getOverlayEntry().remove();
      _isProgressShown = false;
    }
  }
}

class ProgressWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: SizedBox(child: LoadingIndicator(indicatorType: Indicator.ballPulse, color: AppColors.colorPrimary,), height: 50.0,)
    );
    /*return Center(
        child: CupertinoActivityIndicator(
      animating: true,
      radius: 15.0,
    ));*/
  }
}
