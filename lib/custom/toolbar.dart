import 'package:corona_yudiz/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build a toolbar as per app's theme

class Toolbar extends StatelessWidget {
  final String title;
  final String rightIconName;
  final bool showBackIcon;
  final bool showOkButton;
  final GestureTapCallback onPressed;
  const Toolbar({this.title, this.rightIconName, this.showBackIcon = false, this.showOkButton = false,@required this.onPressed });

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 0.0,
      color: Color(0xff111A52),
      child: Container(
        width: AppUtil.getDeviceSize(context).width,
        height: kToolbarHeight,
        margin: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                showBackIcon
                    ? Container(
                        child: IconButton(
                          icon: Icon(Icons.arrow_back_ios),
                          color: Colors.white,
                          onPressed: () {
                            GetIt.I<NavigationUtil>().navigatePop();
                          },
                        ),
                      )
                    : Container(),
                Text(
                  title,
                  style: Styles.tvStyle(
                      fontSize: 24.0,
                      textColor: Colors.white,
                      fontFamily: AppConstants.fontAvenir,
                      fontWeight: FontWeight.w900),
                ),
              ],
            ),
            Spacer(),
            GestureDetector(
              onTap: () => AppUtil.openBrowser("https://t.co/sjZusWJdlP"),
              child: rightIconName != null
                  ? SizedBox(
                      height: 30,
                      width: 30,
                      child: Image.asset(
                        rightIconName,
                      ),
                    )
                  : Container(),
            ),
            GestureDetector(
              onTap: () {
                print("object OK click");
                onPressed();
              },
              child: showOkButton ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                      "OK",
                      style: Styles.tvStyle(
                          fontSize: 15.0,
                          textColor: Colors.white,
                          fontFamily: AppConstants.fontAvenir,
                          fontWeight: FontWeight.w500),
                    ),
              )
                  : Container(),
            ),

          ],
        ),
      ),
    );
  }
}
