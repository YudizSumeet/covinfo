import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:corona_yudiz/custom/regex_input_formatter.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build text input field as per app's theme

class EditText extends StatefulWidget {
  final String hintText;
  final bool isPassword;
  final TextInputType inputType;
  final TextInputAction actionType;
  final bool isOnlyDecimal;
  final Function(String) onSubmitted;
  final TextEditingController textController;
  final TextCapitalization textCapitalization;
  final FocusNode focusNode;
  final bool isEnabled;
  bool errorFlag;

  EditText(
      {@required this.hintText,
      @required this.textController,
      this.focusNode,
      this.onSubmitted,
      this.isOnlyDecimal = false,
      this.isEnabled = true,
      this.textCapitalization = TextCapitalization.none,
      this.isPassword = false,
      this.errorFlag = false,
      this.inputType = TextInputType.text,
      this.actionType = TextInputAction.next});

  @override
  EditTextState createState() => EditTextState();
}

class EditTextState extends State<EditText> {
  _setErrorFlag(bool flag) {
    widget.errorFlag = flag;
    setState(() {});
  }

  _onIconPressed() {
    _setErrorFlag(false);
    widget.textController.text = "";
  }

  _focusListener() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    widget.focusNode.addListener(_focusListener);
  }

  @override
  void dispose() {
    widget.focusNode.removeListener(_focusListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
//    if (widget.errorFlag) FocusScope.of(context).requestFocus(_focusNode);

    return TextField(
        textCapitalization: widget.textCapitalization,
        enabled: widget.isEnabled,
        controller: widget.textController,
        inputFormatters: widget.isOnlyDecimal
            ? [
                RegExInputFormatter.withRegex(
                    '^\$|^(0|([1-9][0-9]{0,}))(\\.[0-9]{0,})?\$')
              ]
            : [],
        onChanged: (text) {
          _setErrorFlag(false);
        },
        onSubmitted: (text) {
          widget.onSubmitted(text);
        },
        maxLines: widget.inputType == TextInputType.multiline ? null : 1,
        obscureText: widget.isPassword,
        focusNode: widget.focusNode,
        keyboardType: widget.inputType,
        textInputAction: widget.actionType,
        cursorColor: AppColors.colorPrimary,
        style: Styles.tvStyle(
            textColor: widget.errorFlag
                ? AppColors.grey_hint
                : AppColors.colorPrimary),
        decoration: Styles.etDecoration(
            isError: widget.errorFlag,
            onIconPressed: _onIconPressed,
            hintText: widget.hintText,
            hintStyle: widget.focusNode.hasFocus
                ? Styles.hintStyle(textColor: Colors.black)
                : Styles.hintStyle(),
            border: widget.focusNode.hasFocus ? null : InputBorder.none));
  }
}
