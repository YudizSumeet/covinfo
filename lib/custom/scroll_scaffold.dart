import 'package:flutter/material.dart';
import 'package:corona_yudiz/custom/scroll.dart';
import 'package:corona_yudiz/custom/toolbar.dart';

/// A common scaffold widget acts a structural base for the screens which can be scrolled when resized

class CustomScrollScaffold extends StatelessWidget {
  final Widget frame;
  final Toolbar toolbar;
  final Color backgroundColor;

  CustomScrollScaffold(
      {@required this.frame,
      this.toolbar,
      this.backgroundColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //          resizeToAvoidBottomPadding: false,
      backgroundColor: backgroundColor,
      body: SafeArea(
        child: Container(
            child: Column(
          children: <Widget>[
            toolbar ?? Container(),
            Expanded(
              child: LayoutBuilder(builder: (context, viewport) {
                return NoOverScroll(
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints:
                          BoxConstraints(minHeight: viewport.maxHeight),
                      child: frame,
                    ),
                  ),
                );
              }),
            ),
          ],
        )),
      ),
    );
  }
}
