import 'package:flutter/material.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build a checkbox as per app's theme

class CheckBox extends StatelessWidget {
  final bool value;
  final Function(dynamic) onChanged;
  final String text;

  CheckBox(
      {@required this.value, @required this.onChanged, @required this.text});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onChanged(value);
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Checkbox(
            value: value,
            onChanged: onChanged,
            activeColor: AppColors.colorPrimary,
          ),
          Text(
            text,
            style: Styles.tvStyle(fontSize: 14.0),
          ),
        ],
      ),
    );
  }
}
