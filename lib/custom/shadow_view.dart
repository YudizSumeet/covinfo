import 'package:flutter/material.dart';

/// A common widget to provide a drop shadow to widgets

class ShadowView extends StatelessWidget {

  final Widget child;
  final EdgeInsets padding;
  final Color backgroundColor;

  ShadowView({@required this.child, this.padding = const EdgeInsets.all(
      0.0), this.backgroundColor = Colors.white});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(color: backgroundColor, boxShadow: [
          BoxShadow(
            color: Colors.black38,
            blurRadius: 5.0,
          )
        ]),
        padding: padding,
        child: child
    );
  }
}
