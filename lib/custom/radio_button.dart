import 'package:flutter/material.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build radio button as per app's theme

class RadioButton extends StatelessWidget {
  final dynamic value, groupValue;
  final Function(dynamic) onChanged;
  final String text;

  RadioButton(
      {@required this.value,
      @required this.groupValue,
      @required this.onChanged,
      @required this.text});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onChanged(value);
      },
      child: Row(
        children: <Widget>[
          Radio(
            activeColor: AppColors.colorPrimary,
            value: value,
            groupValue: groupValue,
            onChanged: (value) {
              onChanged(value);
            },
          ),
          Text(
            text,
            style: Styles.tvStyle(),
          ),
        ],
      ),
    );
  }
}