import 'package:flutter/cupertino.dart';
import 'package:corona_yudiz/values/colors.dart';

/// A widget to handle over scroll of the lists or scrollable views

class OverScroll extends StatelessWidget {
  final Widget child;

  OverScroll({@required this.child});

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: ScrollBehavior(),
      child: GlowingOverscrollIndicator(
        axisDirection: AxisDirection.down,
        color: AppColors.colorPrimary,
        child: child,
      ),
    );
  }
}


class NoOverScroll extends StatelessWidget {
  final Widget child;

  NoOverScroll({@required this.child});

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: CustomScrollBehavior(),
      child: child,
    );
  }
}

class CustomScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
