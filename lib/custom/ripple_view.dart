import 'package:flutter/material.dart';

/// A widget to provide a material ripple effect for clickable widgets like buttons, icons

class RippleView extends StatelessWidget {
  final double radius;
  BorderRadius borderRadius;
  final Color rippleColor;
  final Color backgroundColor;
  final Widget child;
  final Function onClick;

  RippleView(
      {@required this.child,
      @required this.onClick,
      this.radius = 10,
      this.rippleColor,
      this.backgroundColor = Colors.transparent}) {
    borderRadius = BorderRadius.all(Radius.circular(radius));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: backgroundColor,
      borderRadius: borderRadius,
      child: InkWell(
          highlightColor: Colors.transparent,
          borderRadius: borderRadius,
          splashColor: rippleColor ?? Colors.black12,
          onTap: onClick,
          child: child),
    );
  }
}
