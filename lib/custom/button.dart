import 'package:flutter/material.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';

/// A common widget to build a button as per app's theme
///
class Button extends StatelessWidget {
  final String text;
  final Function onClick;
  final Color color;

  Button(
      {@required this.text,
      @required this.onClick,
      this.color = AppColors.colorPrimary});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Container(
        padding: const EdgeInsets.all(14.0),
        alignment: Alignment.center,
        width: AppUtil.getDeviceSize(context).width,
        child: Text(
          text,
          style: Styles.tvStyle(
              fontWeight: FontWeight.w700,
              textColor: Colors.white,
              fontSize: 14.0),
        ),
      ),
      color: color,
      onPressed: onClick,
    );
  }
}
