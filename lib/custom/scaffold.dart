import 'package:flutter/material.dart';
import 'package:corona_yudiz/values/colors.dart';

/// A common scaffold widget acts a structural base for all the screens in the app

class CustomScaffold extends StatelessWidget {
  final Widget frame;
  final Widget  toolbar;
  final Color backgroundColor;
  final Container drawer;
  final bool resize;
  final GlobalKey<ScaffoldState> globalKey;

  CustomScaffold(
      {@required this.frame,
      this.toolbar,
      this.resize = true,
      this.drawer,
      this.globalKey,
      this.backgroundColor = AppColors.colorPrimary});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: globalKey ?? GlobalKey(),
        drawer: drawer != null
            ? SizedBox(
                child: drawer,
                width: 300,
              )
            : null,
        resizeToAvoidBottomPadding: resize,
        backgroundColor: backgroundColor,
        body: SafeArea(
          child: Container(
              child: Column(
            children: <Widget>[
              toolbar ?? Container(),
              Expanded(
                child: frame,
              )
            ],
          )),
        ));
  }
}
