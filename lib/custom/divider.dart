import 'package:flutter/material.dart';
import 'package:corona_yudiz/values/colors.dart';

/// A common widget to build a divider between list items

class AppDivider extends StatelessWidget {
  final Color color;
  final double height;

  AppDivider({this.color = AppColors.app_black, this.height = 1.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: color,
    );
  }
}
