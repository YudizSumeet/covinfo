class ModelMore {
  final String title;
  final String body;

  ModelMore._({this.title, this.body});

  factory ModelMore.fromJson(Map<String, dynamic> json) {
    return new ModelMore._(
      title: json['title'],
      body: json['body'],
    );
  }
}