import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/custom/scaffold.dart';
import 'package:corona_yudiz/custom/toolbar.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailScreen extends BaseScreen {
  final String title, content;

  DetailScreen(this.title, this.content);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends BaseScreenState<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
        toolbar: Toolbar(
          showBackIcon: true,
          title: widget.title,
          onPressed: () {},
        ),
        frame: Container(
          width: AppUtil.getDeviceSize(context).width,
          margin: EdgeInsets.only(bottom: 20, left: 20, right: 20, top: 10),
          decoration: BoxDecoration(
            color: AppColors.app_white,
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
              child: Html(data: widget.content)),
        ));
  }
}
