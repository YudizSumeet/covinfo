import 'dart:convert';

import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/screen/countrypicker/country_list.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/screen/more/detail_screen.dart';
import 'package:corona_yudiz/screen/more/model_more.dart';
import 'package:corona_yudiz/screen/source/source.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:share/share.dart';

import '../../constants.dart';

class MorePage extends BaseScreen {
  @override
  State<MorePage> createState() => _MoreState();
}

class _MoreState extends BaseScreenState<MorePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  List<ModelMore> moreList;

  @override
  void initState() {
    getMoreDetails();
    moreList = List();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
          color: AppColors.app_white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          getMoreWidget(
              "${AppConstants.localImageBasePath}ic_corona_symptoms.png",
              AppLocalizations.of(context).corona_symptoms,
              0),
          getMoreWidget(
              "${AppConstants.localImageBasePath}ic_corona_precocious.png",
              AppLocalizations.of(context).corona_precautions,
              1),
          getMoreWidget(
              "${AppConstants.localImageBasePath}ic_phone_numbers.png",
              AppLocalizations.of(context).emergency_numbers,
              2),
          getMoreWidget("${AppConstants.localImageBasePath}ic_about_us.png",
              AppLocalizations.of(context).about_us, 3),
          getMoreWidget("${AppConstants.localImageBasePath}ic_contact_us.png",
              AppLocalizations.of(context).contact_us, 4),
          getMoreWidget("${AppConstants.localImageBasePath}ic_sources.png",
              "Sources", 5),
          getMoreWidget("${AppConstants.localImageBasePath}ic_country.png",
              AppLocalizations.of(context).settings, 6),
          getMoreWidget("${AppConstants.localImageBasePath}share.png",
              AppLocalizations.of(context).share_app, 7),
          getMoreWidget("${AppConstants.localImageBasePath}privacy_policy.png",
              AppLocalizations.of(context).privacy_policy, 8),
        ],
      ),
    );
  }

  Widget getMoreWidget(String icon, String name, int index) {
    return GestureDetector(
      onTap: () async {
        switch (index) {
          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
            if (moreList.isNotEmpty)
              GetIt.I<NavigationUtil>()
                  .navigatePush(DetailScreen(name, moreList[index].body));
            break;
          case 5:
            GetIt.I<NavigationUtil>().navigatePush(SourcePage());
            break;
          case 6:
            openSelectCountryScreen();
            break;
          case 7:
            final RenderBox box = context.findRenderObject();
            Share.share(AppConstants.SHARE_APP_SUBJECT,
                sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
            break;
          case 8:
            AppUtil.openBrowser(ApiConfig.PRIVACY_POLICY_URL);
            break;
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: Image.asset(
                icon,
                width: 20.0,
                height: 20.0,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: Text(
                name,
                style: Styles.tvStyle(
                    textColor: AppColors.colorPrimary,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w500),
              ),
            )
          ],
        ),
      ),
    );
  }

  void getMoreDetails() async {
    print("MORE VALUE  ");
    Response res = await get(APIEndPoint.MORE);
    List<dynamic> bodyList = jsonDecode(res.body);
    List<ModelMore> moreList =
        bodyList.map((data) => ModelMore.fromJson(data)).toList();

    print("MORE VALUE" + moreList[0].body);
    this.moreList = moreList;
  }

  void openSelectCountryScreen() async {
    Map results = await GetIt.I<NavigationUtil>()
        .navigatePush(CountryListPage(fromSetting: true));

    if (results != null &&
        results.containsKey(AppConstants.EXTRA_SELECTED_COUNTRY)) {
      GetIt.I<NavigationUtil>().pushReplacementNamed(MainScreen(
          results[AppConstants.EXTRA_SELECTED_COUNTRY] as CountryListModel));
    }
  }
}
