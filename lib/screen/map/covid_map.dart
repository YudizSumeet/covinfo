import 'dart:async';
import 'dart:convert';

import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/countrypicker/country_list.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/screen/home/overview.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class ActCovidMap extends StatefulWidget {
  CoronaCasesModel allOverCoronaCases;
  final List<CoronaCasesModel> countryWiseCoronaCases;

  ActCovidMap(
      {@required this.allOverCoronaCases,
      @required this.countryWiseCoronaCases});

  @override
  _ActCovidMapState createState() => _ActCovidMapState();
}

class _ActCovidMapState extends State<ActCovidMap>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Completer<GoogleMapController> _controller = Completer();
  final GlobalKey _KeyOverView = GlobalKey();
  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);
  final Set<Marker> markers = Set();
  final List<CoronaCasesModel> coronaCasesList = List();
  static LatLng pinPosition = LatLng(35.0, 105.0);
  BitmapDescriptor pinLocationIcon;
  final _pageController = PageController(viewportFraction: 0.8);

  static CameraPosition cameraPosition = CameraPosition(
    target: pinPosition,
    zoom: 3.0,
  );

  void _setStyle(GoogleMapController controller) async {
    String value =
        await DefaultAssetBundle.of(context).loadString('assets/map_style.txt');
    controller.setMapStyle(value);
  }

  void onPageChanged(int index) {
    if (mounted && coronaCasesList.length > 0) {
      setState(() {
        _controller.future.then((controller) {
          controller.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(
                      coronaCasesList[index].lat, coronaCasesList[index].long),
                  zoom: 7.0),
            ),
          );
        });
      });
    }
  }

  Future<List<CountryListModel>> getCountryListFromJson() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString(AppConstants.COUNTRY_LIST_JSON_FILE_PATH);
    Map<String, dynamic> parsedJson = json.decode(data);
    return parsedJson["ref_country_codes"]
        .map((item) => CountryListModel.fromJson(item))
        .toList()
        .cast<CountryListModel>();
  }

  @override
  void initState() {
    setCustomMapPin();
    getCountryListFromJson().then((countryList) {
      setState(() {
        for (var index = 0;
            index < widget.countryWiseCoronaCases.length;
            index++) {
          CoronaCasesModel item = widget.countryWiseCoronaCases[index];
          try {
            CountryListModel countryListModel = countryList.singleWhere(
                (element) => element.enShortName == item.countryName);
            item.lat = countryListModel.latitude;
            item.long = countryListModel.longitude;

            coronaCasesList.add(item);
            Marker marker = Marker(
                markerId: MarkerId(item.countryName),
                position: LatLng(item.lat, item.long),
                icon: pinLocationIcon,
                onTap: () {
                  setState(() {
                    var pos = coronaCasesList.indexOf(item);
                    onPageChanged(pos);
                    _pageController.animateToPage(
                      pos,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.slowMiddle,
                    );
                  });
                });
            markers.add(marker);
          } catch (StateError) {}
        }
      });
    });
    super.initState();
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        "${AppConstants.localImageBasePath}ic_marker.png");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: cameraPosition,
            markers: markers,
            onMapCreated: (GoogleMapController controller) {
              onPageChanged(0);
              _setStyle(controller);
              _controller.complete(controller);
            },
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20.0),
                child: OverviewWidget(_KeyOverView, widget.allOverCoronaCases),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                height: 270.0,
                child: PageView.builder(
                  physics: BouncingScrollPhysics(),
                  onPageChanged: onPageChanged,
                  controller: _pageController,
                  itemCount: coronaCasesList.length,
                  itemBuilder: (BuildContext context, int itemIndex) {
                    return rowCountryHolder(context, itemIndex);
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget rowCountryHolder(BuildContext context, int itemIndex) {
    CoronaCasesModel country = coronaCasesList[itemIndex];
    return Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            country.countryName.toUpperCase(),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontFamily: AppConstants.fontAvenir,
              fontSize: 20,
              fontWeight: FontWeight.w900,
              color: AppColors.colorPrimary,
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            decoration: BoxDecoration(
              color: AppColors.red_light,
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(AppLocalizations.of(context).confirm_cases,
                    style: TextStyle(
                        fontFamily: AppConstants.fontAvenir,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white)),
                SizedBox(height: 5),
                Text(
                  formatter.format(country.totalCases),
                  style: TextStyle(
                      fontFamily: AppConstants.fontAvenir,
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                )
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            decoration: BoxDecoration(
              color: AppColors.blue_light,
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(AppLocalizations.of(context).recovered,
                    style: TextStyle(
                        fontFamily: AppConstants.fontAvenir,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white)),
                SizedBox(height: 5),
                Text(
                  formatter.format(country.totalRecovered),
                  style: TextStyle(
                      fontFamily: AppConstants.fontAvenir,
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                )
              ],
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            decoration: BoxDecoration(
              color: AppColors.red_dark,
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(AppLocalizations.of(context).death,
                    style: TextStyle(
                        fontFamily: AppConstants.fontAvenir,
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white)),
                SizedBox(height: 5),
                Text(
                  formatter.format(country.totalDeath),
                  style: TextStyle(
                      fontFamily: AppConstants.fontAvenir,
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      color: Colors.white),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
