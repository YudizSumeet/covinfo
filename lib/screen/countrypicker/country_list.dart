import 'dart:convert';

import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/toolbar.dart';
import 'package:corona_yudiz/screen/countrypicker/search_widget.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CountryListPage extends BaseScreen {
  final bool fromSplash;
  final bool fromSetting;
  CountryListModel selCountryForChart;

  CountryListPage(
      {this.fromSplash = false,
      this.fromSetting = false,
      this.selCountryForChart});

  @override
  State<CountryListPage> createState() => _CountryListState();
}

class _CountryListState extends BaseScreenState<CountryListPage> {
  final searchController = TextEditingController();
  final countryList = List<CountryListModel>();
  final originalCountryList = List<CountryListModel>();

  CountryListModel selCountry;
  SharedPreferences prefs;

//  int selCountryPos = -1;

  initPrefs() async {
    if (!widget.fromSplash && !widget.fromSetting) {
      selCountry = widget.selCountryForChart;
    } else {
      prefs = await getPrefs();
      String modelCountry = (prefs.getString(AppConstants.KEY_SEL_COUNTRY));
      if (modelCountry != null) {
        var parsedJson = json.decode(modelCountry);
        selCountry = CountryListModel.fromJson(parsedJson);
      }
    }
  }

  Future<List<CountryListModel>> getCountryListFromJson() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString(AppConstants.COUNTRY_LIST_JSON_FILE_PATH);
    Map<String, dynamic> parsedJson = json.decode(data);
    return parsedJson["ref_country_codes"]
        .map((item) => CountryListModel.fromJson(item))
        .toList()
        .cast<CountryListModel>();
  }

  @override
  void initState() {
    initPrefs();
    getCountryListFromJson().then((onValue) {
      setState(() {
        if (!widget.fromSplash && !widget.fromSetting) {
          originalCountryList.add(CountryListModel(
              0, AppConstants.ALL_COUNTRY, "", AppConstants.ALL_COUNTRY, 0, 0));
        }
        originalCountryList.addAll(onValue);

        /**
         * if selCountry == null then set default selCountry as India
         */
        if (selCountry == null) {
          /**
           * if selCountry == null then set default selCountry as India
           */
          if (!widget.fromSplash && !widget.fromSetting) {
            selCountry = originalCountryList.singleWhere(
                (item) => item.alpha2Code == AppConstants.ALL_COUNTRY);
          } else {
            selCountry = originalCountryList.singleWhere(
                (item) => item.alpha2Code == AppConstants.INDIA_COUNTRY_CODE);
          }
        } else {
          selCountry = originalCountryList
              .singleWhere((item) => item.alpha2Code == selCountry.alpha2Code);
        }
        selCountry.isSelected = true;
        int selCountryPos = originalCountryList.indexOf(selCountry);
        selCountry = originalCountryList.removeAt(selCountryPos);

        originalCountryList.insert(0, selCountry);

        /**
         * set all country in first
         */
        if (!widget.fromSplash && !widget.fromSetting) {
          int allCountryPos = originalCountryList.indexWhere(
                  (item) => item.alpha2Code == AppConstants.ALL_COUNTRY);
          if (allCountryPos != -1) {
            var allCountry = originalCountryList.removeAt(allCountryPos);
            originalCountryList.insert(0, allCountry);
          }
        }
        countryList.addAll(originalCountryList);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorPrimary,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Toolbar(
              showBackIcon: widget.fromSplash ? false : true,
              title: AppLocalizations.of(context).select_country.toUpperCase(),
              showOkButton: true,
              onPressed: () {
                setState(() {
                  if (widget.fromSplash || widget.fromSetting) {
                    /**
                     * Select country for indian country or not from splash screen
                     */
                    String strCountry = jsonEncode(selCountry.toJson());
                    prefs.setString(AppConstants.KEY_SEL_COUNTRY, strCountry);
                    GetIt.I<NavigationUtil>()
                        .navigatePushReplace(MainScreen(selCountry));
                  } else {
                    /**
                     * Select country for chart value change from home screen
                     */
                    if (selCountry.enShortName == AppConstants.ALL_COUNTRY) {
                      Navigator.of(context)
                          .pop({AppConstants.EXTRA_SELECTED_COUNTRY: null});
                    } else {
                      Navigator.of(context).pop(
                          {AppConstants.EXTRA_SELECTED_COUNTRY: selCountry});
                    }
                  }
                });
              },
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    SearchView(
                        hintText:
                            AppLocalizations.of(context).search_country_data,
                        textController: searchController,
                        onTextChange: (String value) {
                          setState(() {
                            countryList.clear();
                            originalCountryList.forEach((country) => {
                                  if (country.enShortName
                                      .toLowerCase()
                                      .startsWith(value.toLowerCase().trim()))
                                    {countryList.add(country)}
                                });
                          });
                        }),
                    Expanded(child: getCountryList()),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getCountryList() {
    return Container(
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.only(top: 20.0),
      decoration: BoxDecoration(
        color: AppColors.app_white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              physics: BouncingScrollPhysics(),
              itemCount: countryList.length,
              itemBuilder: (context, position) {
                return listItem(position);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget listItem(int position) {
    CountryListModel country = countryList[position];
    return GestureDetector(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5.0),
          padding: EdgeInsets.symmetric(vertical: 8.0),
          decoration: BoxDecoration(
            color: country.isSelected
                ? AppColors.colorPrimary
                : AppColors.light_grayish,
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
          ),
          child: Text(
            country.enShortName.toUpperCase(),
            textAlign: TextAlign.center,
            style: Styles.tvStyle(
                fontSize: 14.0,
                fontFamily: AppConstants.fontAvenir,
                textColor: country.isSelected
                    ? AppColors.app_white
                    : AppColors.colorPrimary,
                fontWeight: FontWeight.w500),
          ),
        ),
        onTap: () {
          setState(() {
            selCountry.isSelected = false;
            country.isSelected = true;
            selCountry = country;
          });
        });
  }
}

class CountryListModel {
  final String enShortName;
  final String alpha2Code;
  final String alpha3Code;
  final int numCode;
  final double latitude;
  final double longitude;
  bool isSelected = false;

  CountryListModel(this.numCode, this.alpha2Code, this.alpha3Code,
      this.enShortName, this.latitude, this.longitude);

  CountryListModel.fromJson(Map jsonMap)
      : enShortName = jsonMap['country'],
        alpha2Code = jsonMap['alpha2'],
        alpha3Code = jsonMap['alpha3'],
        numCode = jsonMap['numeric'],
        latitude = jsonMap['latitude'] + .0,
        longitude = jsonMap['longitude'] + .0;

  Map<String, dynamic> toJson() {
    return {
      "country": this.enShortName,
      "alpha2": this.alpha2Code,
      "alpha3": this.alpha3Code,
      "numeric": this.numCode,
      "latitude": this.latitude,
      "longitude": this.longitude,
      "longitude": this.longitude,
      "isSelected": this.isSelected,
    };
  }
}
