import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';

class SearchView extends StatefulWidget {
  final String hintText;
  final Function(String) onSubmitted;
  final Function(String) onTextChange;
  final TextEditingController textController;
  final FocusNode focusNode;
  final bool isEnabled;

  SearchView(
      {@required this.hintText,
      @required this.textController,
      this.onSubmitted,
      this.onTextChange,
      this.focusNode,
      this.isEnabled = true});

  @override
  _SearchViewState createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      decoration: BoxDecoration(
        color: AppColors.app_white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: TextField(
          textInputAction: TextInputAction.search,
          style: Styles.tvStyle(
            fontSize: 13.0,
          ),
          focusNode: widget.focusNode,
          controller: widget.textController,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(0.0),
            border: InputBorder.none,
            icon: Icon(Icons.search),
            hintText: widget.hintText,
            hintStyle:
                Styles.tvStyle(fontSize: 13.0, fontWeight: FontWeight.w100),
          ),
          onChanged: (text) {
            widget.onTextChange(text);
          },
          onSubmitted: (text) {
            widget.onSubmitted(text);
          }),
    );
  }
}
