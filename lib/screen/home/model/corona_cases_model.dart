import 'package:google_maps_flutter/google_maps_flutter.dart';

class CoronaCasesModel {
  String countryName;
  int totalCases;
  int newCases;
  int totalDeath;
  int newDeath;
  int totalRecovered;
  int activeCases;
  int serious;
  double lat;
  double long;
  String lastupdatedtime;

  CoronaCasesModel(this.countryName, this.totalCases, this.newCases,
      this.totalDeath, this.newDeath, this.totalRecovered, this.activeCases, this.serious);

  CoronaCasesModel.fromJson(String key, List<dynamic> jsonMap)
      : countryName = key,
        totalCases = jsonMap[0],
        newCases = jsonMap[1],
        totalDeath = jsonMap[2],
        newDeath = jsonMap[3],
        totalRecovered = jsonMap[4],
        activeCases = jsonMap[5],
        serious = jsonMap[6];
}