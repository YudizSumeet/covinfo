class StateCoronaModel {
  String stateName;
  int confirmed;
  int recovered;
  int active;
  int death;
  int newCases;
  List<DistrictCoronaModel> districts;

  StateCoronaModel(this.stateName, this.confirmed, this.recovered,
      this.active, this.death,this.newCases);

  StateCoronaModel.fromJson(List<dynamic> jsonMap)
      : stateName = jsonMap[6],
        confirmed = int.parse(jsonMap[1]),
        recovered = int.parse(jsonMap[5]),
        active = int.parse(jsonMap[0]),
        death = int.parse(jsonMap[2]);
}

class DistrictCoronaModel {
  String districtName;
  int confirmed;

  DistrictCoronaModel(this.districtName, this.confirmed);

  DistrictCoronaModel.fromJson(String key, List<dynamic> jsonMap)
      : districtName = key,
        confirmed = jsonMap[0];
}

class CoronaTimeSeriesModel {
  List<String> date = new List<String>();
  List<int> dailyConfirmed = new List<int>();
  List<int> dailyDeceased = new List<int>();
  List<int> dailyRecovered = new List<int>();

  CoronaTimeSeriesModel(this.date, this.dailyConfirmed, this.dailyDeceased, this.dailyRecovered);

//  fromJson(Map<String, dynamic> jsonMap) {
//    date.add(jsonMap["date"]);
//    dailyConfirmed.add(int.parse(jsonMap["dailyconfirmed"]));
//    dailyDeceased.add(int.parse(jsonMap["dailydeceased"]));
//    dailyRecovered.add(int.parse(jsonMap["dailyrecovered"]));
//  }
}