import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OverviewWidget extends StatefulWidget {
  final GlobalKey _keyOverView;
  final CoronaCasesModel coronaCasesModel;

  OverviewWidget(this._keyOverView, this.coronaCasesModel)
      : super(key: _keyOverView);

  @override
  _OverviewWidgetState createState() => _OverviewWidgetState();
}

class _OverviewWidgetState extends State<OverviewWidget> {
  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
          color: AppColors.app_white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: withProgress(),
    );
  }

  Widget withProgress() {
    if (widget.coronaCasesModel == null) {
      return ProgressWidget();
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).total_confirm_cases,
                style:
                    Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w500),
              ),
              Text(
                formatter.format(widget.coronaCasesModel.totalCases),
                style: Styles.tvStyle(
                    fontSize: 16.0,
                    textColor: AppColors.red_light,
                    fontWeight: FontWeight.w900),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).total_recovered,
                style:
                    Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w500),
              ),
              Text(
                formatter.format(widget.coronaCasesModel.totalRecovered),
                style: Styles.tvStyle(
                    fontSize: 16.0,
                    textColor: AppColors.blue_light,
                    fontWeight: FontWeight.w900),
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).total_death,
                style:
                    Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w500),
              ),
              Text(
                formatter.format(widget.coronaCasesModel.totalDeath),
                style: Styles.tvStyle(
                    fontSize: 16.0,
                    textColor: AppColors.red_dark,
                    fontWeight: FontWeight.w900),
              )
            ],
          ),
        ],
      );
    }
  }
}
