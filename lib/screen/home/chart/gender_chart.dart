import 'package:charts_flutter/flutter.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:corona_yudiz/screen/home/home.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/material.dart';

import '../../../constants.dart';

class GenderChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  GenderChart(this.seriesList, {this.animate});

  factory GenderChart.withSampleData() {
    return new GenderChart(
      _createSampleData(),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.PieChart(
      seriesList,
      animate: animate,
      defaultRenderer: new charts.ArcRendererConfig(
          arcWidth: 30,
          arcRendererDecorators: [new charts.ArcLabelDecorator()]),
      behaviors: [
        charts.DatumLegend(
          position: charts.BehaviorPosition.end,
          horizontalFirst: false,
          cellPadding: new EdgeInsets.only(right: 4.0, bottom: 4.0),
          showMeasures: true,
          legendDefaultMeasure: charts.LegendDefaultMeasure.firstValue,
          measureFormatter: (num value) {
            return value == null ? '-' : '$value%';
          },
          entryTextStyle: new charts.TextStyleSpec(
            fontSize: 11,
            fontFamily: AppConstants.fontRoboto,
            color: charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
          ),
        ),
      ],
    );
  }

  static List<charts.Series<GenderSegment, String>> _createSampleData() {
    int criticalPer = 0;
    int mildPer = 0;
    if (HomePage.countryOfNoOfCases == null) {
//        criticalPer = (MainScreen.allOverCoronaCases.serious * 100 / MainScreen.allOverCoronaCases.activeCases).round();
      mildPer = 100 - criticalPer;
    } else {
//      CoronaCasesModel casesModel = CoronaCasesModel.fromJson(HomePage.countryOfNoOfCases.enShortName, MainScreen.snapData[HomePage.countryOfNoOfCases.enShortName]);
//      criticalPer = (casesModel.serious * 100 / casesModel.activeCases).round();
      mildPer = 100 - criticalPer;
    }
    final data = [
      new GenderSegment(
        'Critical',
        criticalPer,
        charts.ColorUtil.fromDartColor(AppColors.red_dark),
      ),
      new GenderSegment(
        'Mild',
        mildPer,
        charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
      ),
    ];

    return [
      new charts.Series<GenderSegment, String>(
        id: 'Segments',
        colorFn: (GenderSegment segment, _) => segment.color,
        domainFn: (GenderSegment segment, _) => segment.segment,
        measureFn: (GenderSegment segment, _) => segment.size,
        labelAccessorFn: (GenderSegment row, _) => '${row.size}%',
        data: data,
      )
    ];
  }
}

class GenderSegment {
  final String segment;
  final int size;
  final Color color;

  GenderSegment(this.segment, this.size, this.color);
}
