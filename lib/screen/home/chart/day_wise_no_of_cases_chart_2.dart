import 'package:corona_yudiz/screen/home/home.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/line_chart.dart';
import 'package:mp_chart/mp/controller/line_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/axis/axis_base.dart';
import 'package:mp_chart/mp/core/data/line_data.dart';
import 'package:mp_chart/mp/core/data_interfaces/i_line_data_set.dart';
import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
import 'package:mp_chart/mp/core/description.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';
import 'package:mp_chart/mp/core/enums/legend_form.dart';
import 'package:mp_chart/mp/core/enums/x_axis_position.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';
import 'package:mp_chart/mp/core/value_formatter/value_formatter.dart';

class DayWiseNoOfCasesChart2 extends StatefulWidget {
  DayWiseNoOfCasesChart2();

  @override
  State<StatefulWidget> createState() {
    return DayWiseNoOfCasesChart2State();
  }
}

class DayWiseNoOfCasesChart2State extends State<DayWiseNoOfCasesChart2> {
  LineChartController controller;
  static TypeFace EXTRA_BOLD =
      TypeFace(fontFamily: "Roboto", fontWeight: FontWeight.w900);

  @override
  void initState() {
    _initController();
    _initLineData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _initLineChart();
  }

  void _initController() {
    var desc = Description()..enabled = false;

    controller = LineChartController(
        axisLeftSettingFunction: (axisLeft, controller) {
          axisLeft
            ..drawAxisLine = true
            ..drawLabels = true
            ..drawGridLinesBehindData = false
            ..drawLimitLineBehindData = false
            ..drawGridLines = false;
        },
        axisRightSettingFunction: (axisRight, controller) {
          axisRight.enabled = (false);
        },
        legendSettingFunction: (legend, controller) {
          legend.shape = (LegendForm.LINE);
        },
        xAxisSettingFunction: (xAxis, controller) {
          xAxis
            ..drawAxisLine = true
            ..drawLabels = true
            ..position = XAxisPosition.BOTTOM
            ..drawGridLinesBehindData = false
            ..drawLimitLineBehindData = false
            ..drawGridLines = false
            ..setAxisMinValue(5.0)
            ..labelRotationAngle = 45
            ..setValueFormatter(YAxisFormatter());
        },
        drawGridBackground: false,
        backgroundColor: ColorUtils.WHITE,
        dragXEnabled: true,
        dragYEnabled: true,
        scaleXEnabled: true,
        scaleYEnabled: true,
        minOffset: 0.0,
        extraTopOffset: 20.0,
        extraBottomOffset: -30.0,
        extraLeftOffset: -10.0,
        extraRightOffset: 20.0,
        pinchZoomEnabled: true,
        drawMarkers: true,
        drawBorders: false,
        description: desc);
  }

  void _initLineData() async {
    List<Entry> totalCasesValues = List();
    var lastIndex = HomePage.dayWiseCoronaCases.length;

    for (int index = 0; index < lastIndex; index++) {
      double val = HomePage.dayWiseCoronaCases[index].totalCases.toDouble();
      totalCasesValues.add(Entry(x: index.toDouble(), y: val));
    }

    LineDataSet set;

    set = LineDataSet(totalCasesValues, null);

    set.setDrawIcons(true);
    set.setFillAlpha(0);
    set.setDrawCircles(false);
    set.setDrawHighlightIndicators(false);
    set.setDrawValues(false);

    set.setColor1(AppColors.red_light);
    set.setLineWidth(2.0);
    set.setForm(null);
    set.setDrawFilled(true);

    List<ILineDataSet> dataSets = List();
    dataSets.add(set);

    List<Entry> totalRecoveredValues = List();
    for (int index = 0; index < lastIndex; index++) {
      double val = HomePage.dayWiseCoronaCases[index].totalRecovered.toDouble();
      totalRecoveredValues.add(Entry(x: index.toDouble(), y: val));
    }

    set = LineDataSet(totalRecoveredValues, null);

    set.setDrawIcons(true);
    set.setFillAlpha(0);
    set.setDrawCircles(false);
    set.setDrawHighlightIndicators(false);
    set.setDrawValues(false);

    set.setColor1(AppColors.blue_light);
    set.setLineWidth(2.0);
    set.setForm(null);
    set.setDrawFilled(true);

    dataSets.add(set);

    List<Entry> totalDeathValues = List();
    for (int index = 0; index < lastIndex; index++) {
      double val = HomePage.dayWiseCoronaCases[index].totalDeath.toDouble();
      totalDeathValues.add(Entry(x: index.toDouble(), y: val));
    }

    set = LineDataSet(totalDeathValues, null);

    set.setDrawIcons(true);
    set.setFillAlpha(0);
    set.setDrawCircles(false);
    set.setDrawHighlightIndicators(false);
    set.setDrawValues(false);

    set.setColor1(AppColors.red_dark);
    set.setLineWidth(2.0);
    set.setForm(null);
    set.setDrawFilled(true);

    dataSets.add(set);

    controller.data = LineData.fromList(dataSets);
    setState(() {});
  }

  Widget _initLineChart() {
    var lineChart = LineChart(controller);
    controller.animator
      ..reset()
      ..animateX1(1500);
    return lineChart;
  }
}

class YAxisFormatter extends ValueFormatter {
  @override
  String getAxisLabel(double value, AxisBase axis) {
    return HomePage.dayWiseCoronaCases[value.toInt()].countryName;
  }
}
