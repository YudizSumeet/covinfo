import 'package:charts_flutter/flutter.dart' as charts;
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/values/colors.dart';

/// Forward hatch pattern bar chart example.
///
/// The second series of bars is rendered with a pattern by defining a
/// fillPatternFn mapping function.
import 'package:flutter/material.dart';

class NoOfCasesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  NoOfCasesChart(this.seriesList, {this.animate});

  factory NoOfCasesChart.withSampleData(
      List<CoronaCasesModel> countryWiseCoronaCases) {
    return new NoOfCasesChart(
      _createSampleData(countryWiseCoronaCases),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      barGroupingType: charts.BarGroupingType.grouped,
      behaviors: [
        charts.SeriesLegend(
          outsideJustification: charts.OutsideJustification.start,
          horizontalFirst: true,
          entryTextStyle: new charts.TextStyleSpec(
            fontSize: 11,
            color: charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
          ),
        ),
        charts.SlidingViewport(),
        charts.PanAndZoomBehavior(),
      ],
      domainAxis: new charts.OrdinalAxisSpec(
          viewport: new charts.OrdinalViewport('AePS', 5),
          renderSpec: new charts.SmallTickRendererSpec(
            labelStyle: new charts.TextStyleSpec(
              fontSize: 10,
              fontFamily: AppConstants.fontRoboto,
              color: charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
            ),
          )),
      primaryMeasureAxis: new charts.NumericAxisSpec(
          renderSpec: new charts.GridlineRendererSpec(
        labelStyle: new charts.TextStyleSpec(
          fontSize: 10,
          fontFamily: AppConstants.fontRoboto,
          color: charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
        ),
      )),
    );
  }

  static List<charts.Series<CoronaCasesModel, String>> _createSampleData(
      List<CoronaCasesModel> countryWiseCoronaCases) {
    return [
      new charts.Series<CoronaCasesModel, String>(
        id: 'Confirmed',
        colorFn: (CoronaCasesModel segment, _) =>
            charts.ColorUtil.fromDartColor(AppColors.red_light),
        domainFn: (CoronaCasesModel caseModel, _) => caseModel.countryName,
        measureFn: (CoronaCasesModel caseModel, _) => caseModel.totalCases,
        data: countryWiseCoronaCases,
      ),
      new charts.Series<CoronaCasesModel, String>(
        id: 'Recovered',
        colorFn: (CoronaCasesModel segment, _) =>
            charts.ColorUtil.fromDartColor(AppColors.blue_light),
        domainFn: (CoronaCasesModel caseModel, _) => caseModel.countryName,
        measureFn: (CoronaCasesModel caseModel, _) => caseModel.totalRecovered,
        data: countryWiseCoronaCases,
        fillPatternFn: (CoronaCasesModel caseModel, _) =>
            charts.FillPatternType.solid,
      ),
      new charts.Series<CoronaCasesModel, String>(
        id: 'Death',
        colorFn: (CoronaCasesModel segment, _) =>
            charts.ColorUtil.fromDartColor(AppColors.red_dark),
        domainFn: (CoronaCasesModel caseModel, _) => caseModel.countryName,
        measureFn: (CoronaCasesModel caseModel, _) => caseModel.totalDeath,
        data: countryWiseCoronaCases,
      ),
    ];
  }
}
