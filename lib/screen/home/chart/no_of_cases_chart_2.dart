import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/bar_chart.dart';
import 'package:mp_chart/mp/controller/bar_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/common_interfaces.dart';
import 'package:mp_chart/mp/core/data/bar_data.dart';
import 'package:mp_chart/mp/core/data_set/bar_data_set.dart';
import 'package:mp_chart/mp/core/description.dart';
import 'package:mp_chart/mp/core/entry/bar_entry.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';
import 'package:mp_chart/mp/core/enums/legend_horizontal_alignment.dart';
import 'package:mp_chart/mp/core/enums/legend_orientation.dart';
import 'package:mp_chart/mp/core/enums/legend_vertical_alignment.dart';
import 'package:mp_chart/mp/core/enums/x_axis_position.dart';
import 'package:mp_chart/mp/core/highlight/highlight.dart';
import 'package:mp_chart/mp/core/value_formatter/large_value_formatter.dart';
import 'package:mp_chart/mp/core/value_formatter/value_formatter.dart';

class BarChartMultiple extends StatefulWidget {
  final List<CoronaCasesModel> countryWiseCoronaCases;

  BarChartMultiple(this.countryWiseCoronaCases);

  @override
  State<StatefulWidget> createState() {
    return BarChartMultipleState();
  }
}

class BarChartMultipleState extends State<BarChartMultiple>
    implements OnChartValueSelectedListener {
  BarChartController controller;
  static TypeFace LIGHT =
      TypeFace(fontFamily: "Roboto", fontWeight: FontWeight.w300);

  @override
  void initState() {
    _iniController();
    _initBarData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: getBody());
  }

  Widget getBody() {
    return Stack(
      children: <Widget>[
        Positioned(
          right: 0,
          left: 0,
          top: 0,
          bottom: 0,
          child: isDataInitial
              ? Padding(
                  padding: const EdgeInsets.only(top: 16.0),
                  child: BarChart(controller),
                )
              : Text("data is not initial"),
        ),
      ],
    );
  }

  void _iniController() {
    var desc = Description()..enabled = false;
    double groupSpace = 0.2;
    double barSpace = 0.03;
    controller = BarChartController(
        highlightPerDragEnabled: false,
        highLightPerTapEnabled: false,
        axisLeftSettingFunction: (axisLeft, controller) {
          ValueFormatter formatter = LargeValueFormatter();
          axisLeft
            ..typeface = LIGHT
            ..setValueFormatter(formatter)
            ..drawGridLines = (false)
            ..spacePercentTop = (35)
            ..setAxisMinimum(0);
        },
        axisRightSettingFunction: (axisRight, controller) {
          axisRight.enabled = (false);
        },
        legendSettingFunction: (legend, controller) {
          legend
            ..verticalAlignment = (LegendVerticalAlignment.TOP)
            ..horizontalAlignment = (LegendHorizontalAlignment.LEFT)
            ..orientation = (LegendOrientation.HORIZONTAL)
            ..drawInside = false
            ..typeface = LIGHT
            ..yOffset = (0.0)
            ..xOffset = (0.0)
            ..yEntrySpace = (0)
            ..textSize = (8);
        },
        xAxisSettingFunction: (xAxis, controller) {
          xAxis
            ..typeface = LIGHT
            ..position = XAxisPosition.BOTTOM
            ..drawGridLines = false
            ..setLabelCount1(4)
            ..setAxisMinimum(0.0)
//            ..setAxisMaximum(5.0)
            ..labelRotationAngle = 45
            ..labelRotatedHeight = 10
            ..setValueFormatter(xAxisValueFormatter(widget.countryWiseCoronaCases));
          (controller as BarChartController)
              .groupBars(0.0, groupSpace, barSpace);
        },
        drawGridBackground: false,
        dragXEnabled: true,
        dragYEnabled: true,
        scaleXEnabled: true,
        scaleYEnabled: true,
        pinchZoomEnabled: true,
        minOffset: 0.0,
        extraTopOffset: 20.0,
        extraBottomOffset: 0.0,
        extraLeftOffset: -10.0,
        extraRightOffset: 20.0,
        selectionListener: this,
        drawBarShadow: false,
        drawValueAboveBar: true,
        drawMarkers: false,
        description: desc);

    controller.setScaleMinima(50.0, 0.0);
  }

  bool isDataInitial = false;

  void _initBarData() async {
    List<BarEntry> confirmedCases = List();
    List<BarEntry> recoveredCases = List();
    List<BarEntry> deathCases = List();

    for (int index = 0; index < widget.countryWiseCoronaCases.length; index++) {
      var coronaCase = widget.countryWiseCoronaCases[index];
      confirmedCases.add(
          BarEntry(x: index.toDouble(), y: coronaCase.totalCases.toDouble()));
      recoveredCases.add(BarEntry(
          x: index.toDouble(), y: coronaCase.totalRecovered.toDouble()));
      deathCases.add(
          BarEntry(x: index.toDouble(), y: coronaCase.totalDeath.toDouble()));
    }

    BarDataSet set1, set2, set3;

    set1 = BarDataSet(confirmedCases, "Confirmed");
    set1.setColor1(AppColors.red_light);
    set1.setValueTextColor(AppColors.red_light);
    set1.setValueTypeface(TypeFace(
      fontFamily: AppConstants.fontAvenir,
      fontWeight: FontWeight.w400,
    ));

    set2 = BarDataSet(recoveredCases, "Recovered");
    set2.setColor1(AppColors.blue_light);
    set2.setValueTextColor(AppColors.blue_light);
    set2.setValueTypeface(TypeFace(
      fontFamily: AppConstants.fontAvenir,
      fontWeight: FontWeight.w400,
    ));

    set3 = BarDataSet(deathCases, "Death");
    set3.setColor1(AppColors.red_dark);
    set3.setValueTextColor(AppColors.red_dark);
    set3.setValueTypeface(TypeFace(
      fontFamily: AppConstants.fontAvenir,
      fontWeight: FontWeight.w400,
    ));

    controller.data = BarData(List()..add(set1)..add(set2)..add(set3));
    controller.data
      ..setValueFormatter(LargeValueFormatter())
      ..setValueTypeface(LIGHT)
      ..barWidth = (0.2);

    controller.animator
      ..reset()
      ..animateY1(500);
    setState(() {
      isDataInitial = true;
    });
  }

  @override
  void onNothingSelected() {}

  @override
  void onValueSelected(Entry e, Highlight h) {}
}

class xAxisValueFormatter extends ValueFormatter {
  final List<CoronaCasesModel> countryWiseCoronaCases;

  xAxisValueFormatter(this.countryWiseCoronaCases);

  @override
  String getFormattedValue1(double value) {
    if (value.isNegative) return "";
    return countryWiseCoronaCases[value.toInt()]
        .countryName
        .replaceAll(" ", "\n");
  }
}
