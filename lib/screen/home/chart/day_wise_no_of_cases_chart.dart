import 'package:charts_flutter/flutter.dart' as charts;
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/screen/home/home.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/util/date_time_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:flutter/material.dart';

class DayWiseNoOfCasesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DayWiseNoOfCasesChart(this.seriesList, {this.animate});

  factory DayWiseNoOfCasesChart.withSampleData() {
    return new DayWiseNoOfCasesChart(
      _createSampleData(),
      animate: false,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      defaultRenderer: new charts.LineRendererConfig(includePoints: true),
      behaviors: [
        charts.SeriesLegend(
          outsideJustification: charts.OutsideJustification.start,
          horizontalFirst: true,
          entryTextStyle: new charts.TextStyleSpec(
            fontSize: 11,
            color: charts.ColorUtil.fromDartColor(AppColors.colorPrimary),
          ),
        ),
        charts.SlidingViewport(),
        charts.PanAndZoomBehavior()
      ],
      domainAxis: charts.DateTimeAxisSpec(
        viewport: new charts.DateTimeExtents( start: DateTimeUtil.getDateFromString(dateString: HomePage.dayWiseCoronaCases[0].countryName, format: AppConstants.DATE_FORMAT), end: DateTimeUtil.getDateFromString(dateString: HomePage.dayWiseCoronaCases[15].countryName, format: AppConstants.DATE_FORMAT)),
        tickFormatterSpec: charts.AutoDateTimeTickFormatterSpec(
          day: charts.TimeFormatterSpec(
            format: AppConstants.DATE_FORMAT,
            transitionFormat: AppConstants.DATE_FORMAT,
          ),
        ),
      ),
    );
  }

  static List<charts.Series<CoronaCasesModel, DateTime>> _createSampleData() {
    return [
      new charts.Series<CoronaCasesModel, DateTime>(
        id: 'Confirmed',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.red_light),
        domainFn: (CoronaCasesModel sales, _) => DateTimeUtil.getDateFromString(dateString: sales.countryName, format: AppConstants.DATE_FORMAT),
        measureFn: (CoronaCasesModel sales, _) => sales.totalCases,
        data: HomePage.dayWiseCoronaCases,
      ),
      new charts.Series<CoronaCasesModel, DateTime>(
        id: 'Recovered',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.blue_light),
        domainFn: (CoronaCasesModel sales, _) => DateTimeUtil.getDateFromString(dateString: sales.countryName, format: AppConstants.DATE_FORMAT),
        measureFn: (CoronaCasesModel sales, _) => sales.totalRecovered,
        data: HomePage.dayWiseCoronaCases,
      ),
      new charts.Series<CoronaCasesModel, DateTime>(
        id: 'Death',
        colorFn: (_, __) => charts.ColorUtil.fromDartColor(AppColors.red_dark),
        domainFn: (CoronaCasesModel sales, _) => DateTimeUtil.getDateFromString(dateString: sales.countryName, format: AppConstants.DATE_FORMAT),
        measureFn: (CoronaCasesModel sales, _) => sales.totalDeath,
        data: HomePage.dayWiseCoronaCases,
      )
    ];
  }
}
