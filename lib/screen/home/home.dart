import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/scrap/scrapping.dart';
import 'package:corona_yudiz/screen/countrydata/time_series_chart.dart';
import 'package:corona_yudiz/screen/countrypicker/country_list.dart';
import 'package:corona_yudiz/screen/countrypicker/search_widget.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/screen/home/chart/day_wise_no_of_cases_chart.dart';
import 'package:corona_yudiz/screen/home/chart/day_wise_no_of_cases_chart_2.dart';
import 'package:corona_yudiz/screen/home/chart/no_of_cases_chart.dart';
import 'package:corona_yudiz/screen/home/chart/no_of_cases_chart_2.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/screen/home/overview.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomePage extends BaseScreen {
  static CountryListModel countryOfNoOfCases;
  static List<CoronaCasesModel> dayWiseCoronaCases = List();

  CoronaCasesModel allOverCoronaCases;
  final List<CoronaCasesModel> countryWiseCoronaCases;
  final onDataChange;

  HomePage(
      {@required this.allOverCoronaCases,
      @required this.countryWiseCoronaCases,
      @required this.onDataChange});

  @override
  State<HomePage> createState() => HomePageState();
}

class HomePageState extends BaseScreenState<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  Dimens _dimens;
  bool isDataNotFound = false;

  Future<Map<String, List<dynamic>>> getAllData;

  final GlobalKey _keyOverView = GlobalKey();
  final GlobalKey _keyCoronaChart = GlobalKey();
  final searchController = TextEditingController();
  final countryList = List<CoronaCasesModel>();
  final searchFocusNode = FocusNode();
  final scrollController = new ScrollController();
  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);

  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    CoronaCasesModel allOverCoronaCases;
    List<CoronaCasesModel> countryWiseCoronaCases = List();
    getAllData.then((snapData) {
      _refreshController.refreshCompleted();
      setState(() {
        MainScreen.snapData = snapData;
        allOverCoronaCases = CoronaCasesModel.fromJson(
            AppConstants.KEY_GLOBAL, snapData[AppConstants.KEY_GLOBAL]);

        countryWiseCoronaCases.clear();
        snapData.forEach((key, value) {
          if (key != AppConstants.KEY_GLOBAL) {
            CoronaCasesModel model =
                CoronaCasesModel.fromJson(key, snapData[key]);
            countryWiseCoronaCases.add(model);
          }
        });
        countryWiseCoronaCases.sort((CoronaCasesModel a, CoronaCasesModel b){
          return b.totalCases.compareTo(a.totalCases);
        });
        widget.onDataChange(allOverCoronaCases, countryWiseCoronaCases);
      });
    });
  }

  @override
  void initState() {
    getAllData = Scrapping().getAllData();
    searchFocusNode.addListener(() {
      scrollToSearchResult();
    });
    if (widget.allOverCoronaCases == null) {
      _onRefresh();
    }
    countryList.clear();
    countryList.addAll(widget.countryWiseCoronaCases);
    super.initState();
  }

  @override
  void dispose() {
    searchFocusNode.dispose();
    super.dispose();
  }

  void scrollToSearchResult() {
    RenderBox renderBoxOverview =
        _keyOverView.currentContext.findRenderObject();
    RenderBox renderBoxCoronaChart =
        _keyCoronaChart.currentContext.findRenderObject();
    scrollController.animateTo(
        renderBoxOverview.size.height + renderBoxCoronaChart.size.height + 20.0,
        duration: Duration(milliseconds: 300),
        curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: SearchView(
              hintText: AppLocalizations.of(context).search_country_data,
              textController: searchController,
              onTextChange: (String value) {
                setState(() {
                  countryList.clear();
                  widget.countryWiseCoronaCases.forEach((item) => {
                        if (item.countryName
                            .toLowerCase()
                            .startsWith(value.toLowerCase().trim()))
                          {countryList.add(item)}
                      });
                });
              },
              focusNode: searchFocusNode,
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: SmartRefresher(
              child: ListView(
                physics: BouncingScrollPhysics(),
                shrinkWrap: true,
                controller: scrollController,
                children: <Widget>[
                  OverviewWidget(_keyOverView, widget.allOverCoronaCases),
                  getCoronaCasesContainer(),
                  getCountryList(),
                ],
              ),
              header: WaterDropMaterialHeader(
                backgroundColor: AppColors.colorPrimary,
              ),
              controller: _refreshController,
              onRefresh: _onRefresh,
            ),
          ),
        ],
      ),
    );
  }

  Widget getCoronaCasesContainer() {
    return Container(
      key: _keyCoronaChart,
      padding: EdgeInsets.all(20.0),
      margin: EdgeInsets.only(top: 20.0),
      decoration: BoxDecoration(
          color: AppColors.app_white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).number_of_cases,
                style: Styles.tvStyle(
                  fontSize: 13.0,
                  fontFamily: AppConstants.fontRoboto,
                  fontWeight: FontWeight.w100,
                ),
              ),
              GestureDetector(
                child: Row(
                  children: <Widget>[
                    Text(
                      HomePage.countryOfNoOfCases != null
                          ? HomePage.countryOfNoOfCases.enShortName
                          : AppConstants.ALL_COUNTRY,
                      style: Styles.tvStyle(
                        fontSize: 10.0,
                        fontFamily: AppConstants.fontRoboto,
                        fontWeight: FontWeight.w100,
                      ),
                    ),
                    Icon(
                      Icons.keyboard_arrow_down,
                      size: 16.0,
                    )
                  ],
                ),
                onTap: () async {
                  Map results = await GetIt.I<NavigationUtil>()
                      .navigatePush(CountryListPage(selCountryForChart: HomePage.countryOfNoOfCases,));

                  if (results != null &&
                      results
                          .containsKey(AppConstants.EXTRA_SELECTED_COUNTRY)) {
                    HomePage.countryOfNoOfCases =
                        results[AppConstants.EXTRA_SELECTED_COUNTRY];
                  }
                },
              ),
            ],
          ),
          SizedBox(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: getCoronaCasesChartWithProgress()),
        ],
      ),
    );
  }

  Widget getCoronaCasesChartWithProgress() {
    if (HomePage.countryOfNoOfCases == null) {
      if (widget.allOverCoronaCases == null) {
        return ProgressWidget();
      } else {
        return getCoronaCasesChartWithNoData();
      }
    } else {
      return FutureBuilder(
          future: Scrapping().getCountryData(
              MainScreen.snapData, HomePage.countryOfNoOfCases.enShortName),
          builder: (BuildContext context, AsyncSnapshot snapshot) {

            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                errorToast(
                    message: AppLocalizations.of(context)
                        .e_msg_something_went_worng);
              }

              if (snapshot.data != null) {
                (snapshot.data as Map<List<dynamic>, List<dynamic>>)
                    .forEach((key, value) {
                  HomePage.dayWiseCoronaCases.clear();
                  for (var index = 0; index < key.length; index++) {
                    HomePage.dayWiseCoronaCases.add(CoronaCasesModel(
                        key[index],
                        value[0][index],
                        0,
                        value[2][index],
                        0,
                        value[1][index],
                        0,
                        0));
                  }
                });
              } else {
                isDataNotFound = true;
              }

              return getCoronaCasesChartWithNoData();
            } else {
              return ProgressWidget();
            }
          });
    }
  }

  Widget getCoronaCasesChartWithNoData() {
    if (isDataNotFound) {
      return getNoDataFound();
    }
    if (HomePage.countryOfNoOfCases == null) {
      return BarChartMultiple(widget.countryWiseCoronaCases);
//      return NoOfCasesChart.withSampleData(widget.countryWiseCoronaCases);
    } else {
      return DayWiseNoOfCasesChart2();
//      return DayWiseNoOfCasesChart.withSampleData();
    }
  }

  Widget getNoDataFound() {
    return Center(child: Text(AppLocalizations.of(context).no_data_found));
  }

  Widget getCountryList() {
    return Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.only(top: 20.0),
      decoration: BoxDecoration(
        color: AppColors.app_white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              AppLocalizations.of(context).all_country_data,
              style: Styles.tvStyle(fontSize: 14.0, fontWeight: FontWeight.w100),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text(AppLocalizations.of(context).country,
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(fontSize: _dimens.t10))),
                Expanded(
                    child: Text(AppLocalizations.of(context).total_cases,
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(fontSize: _dimens.t10))),
                Expanded(
                    child: Text(AppLocalizations.of(context).active_cases,
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(fontSize: _dimens.t10))),
                Expanded(
                    child: Text(AppLocalizations.of(context).deaths,
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(fontSize: _dimens.t10))),
                Expanded(
                    child: Text(AppLocalizations.of(context).recovered,
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(fontSize: _dimens.t10))),
              ],
            ),
          ),
          getCountryListWithProgress(),
        ],
      ),
    );
  }

  Widget getCountryListWithProgress() {
    if (widget.allOverCoronaCases == null) {
      return ProgressWidget();
    } else {
      return Flexible(
        fit: FlexFit.loose,
        child: ListView.builder(
          shrinkWrap: true,
          physics: BouncingScrollPhysics(),
          itemCount: countryList.length,
          itemBuilder: (context, position) {
            return listItem(position);
          },
        ),
      );
    }
  }

  Widget listItem(int position) {
    CoronaCasesModel country = countryList[position];
    return Container(
      margin: EdgeInsets.only(top: 4.0, bottom: 4.0),
      decoration: BoxDecoration(
          color: AppColors.grey_light,
          borderRadius: BorderRadius.all(Radius.circular(2.0))),
      padding: const EdgeInsets.all(5.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                  child: Text(
                    country.countryName,
                    style: Styles.tvStyle(
                        fontSize: _dimens.t13,
                        textColor: AppColors.textColorDarkBlue,
                        fontWeight: FontWeight.w900),
                  )),
              Text(
                "+ ${formatter.format(country.newCases)} New Cases",
                style: Styles.tvStyle(
                    fontStyle: FontStyle.italic,
                    fontSize: _dimens.t10,
                    textColor: AppColors.textColorDarkBlue,
                    fontWeight: FontWeight.w900),
              ),
            ],
          ),
          SizedBox(height: 5,),
          Row(
            children: <Widget>[
              Expanded(child: Container(),),
              Expanded(
                  child: Text(
                    formatter.format(country.totalCases),
                    textAlign: TextAlign.center,
                    style: Styles.tvStyle(
                        fontSize: _dimens.t12,
                        textColor: AppColors.red_light,
                        fontWeight: FontWeight.w900),
                  )),
              Expanded(
                  child: Text(
                    formatter.format(country.activeCases),
                    textAlign: TextAlign.center,
                    style: Styles.tvStyle(
                        fontSize: _dimens.t12,
                        textColor: AppColors.yellow_dark,
                        fontWeight: FontWeight.w900),
                  )),
              Expanded(
                  child: Text(
                    formatter.format(country.totalDeath),
                    textAlign: TextAlign.center,
                    style: Styles.tvStyle(
                        fontSize: _dimens.t12,
                        textColor: AppColors.red_dark,
                        fontWeight: FontWeight.w900),
                  )),
              Expanded(
                  child: Text(
                    formatter.format(country.totalRecovered),
                    textAlign: TextAlign.center,
                    style: Styles.tvStyle(
                        fontSize: _dimens.t12,
                        textColor: AppColors.blue_light,
                        fontWeight: FontWeight.w900),
                  )),
            ],
          ),
        ],
      ),
    );
  }
}