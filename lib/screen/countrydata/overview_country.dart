import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/screen/countrydata/time_series_chart.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/screen/home/model/state_model.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class OverviewCountryWidget extends StatefulWidget {
  final GlobalKey _keyOverView;
  final CoronaCasesModel coronaCasesModel;
  final CoronaTimeSeriesModel coronaTimeSeriesData;

  OverviewCountryWidget(this._keyOverView, this.coronaCasesModel, {this.coronaTimeSeriesData})
      : super(key: _keyOverView);

  @override
  _OverviewCountryWidgetState createState() => _OverviewCountryWidgetState();
}

class _OverviewCountryWidgetState extends State<OverviewCountryWidget> {
  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
          color: AppColors.app_white,
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: withProgress(),
    );
  }

  Widget withProgress() {
    if (widget.coronaCasesModel == null) {
      return ProgressWidget();
    } else {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).total_cases_in_india,
                style:
                Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w900),
              ),
              Text(
                "(" +formatter.format(widget.coronaCasesModel.activeCases) + ") " + AppLocalizations.of(context).active_cases,
                style: Styles.tvStyle(
                    fontSize: 11.0,
                    textColor: AppColors.colorPrimary,
                    fontWeight: FontWeight.w900),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0, right: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).confirmed,
                      style:
                      Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w900),
                    ),
                    Text(
                      formatter.format(widget.coronaCasesModel.totalCases),
                      style: Styles.tvStyle(
                          fontSize: 16.0,
                          textColor: AppColors.red_light,
                          fontWeight: FontWeight.w900),
                    ),
                    getConfirmedChart(),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).recovered,
                      style:
                      Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w900),
                    ),
                    Text(
                      formatter.format(widget.coronaCasesModel.totalRecovered),
                      style: Styles.tvStyle(
                          fontSize: 16.0,
                          textColor: AppColors.blue_light,
                          fontWeight: FontWeight.w900),
                    ),
                    getRecoveredChart(),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      AppLocalizations.of(context).death,
                      style:
                      Styles.tvStyle(fontSize: 11.0, fontWeight: FontWeight.w900),
                    ),
                    Text(
                      formatter.format(widget.coronaCasesModel.totalDeath),
                      style: Styles.tvStyle(
                          fontSize: 16.0,
                          textColor: AppColors.red_dark,
                          fontWeight: FontWeight.w900),
                    ),
                    getDeathChart(),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
  
  Widget getConfirmedChart() {
    if (widget.coronaTimeSeriesData != null) {
      return SizedBox(child: TimeSeriesChart(AppColors.red_light, widget.coronaTimeSeriesData.dailyConfirmed), width: 50.0, height: 30.0, );
    } else {
      return SizedBox();
    }
  }

  Widget getRecoveredChart() {
    if (widget.coronaTimeSeriesData != null) {
      return SizedBox(child: TimeSeriesChart(AppColors.blue_light, widget.coronaTimeSeriesData.dailyRecovered), width: 50.0, height: 30.0, );
    } else {
      return SizedBox();
    }
  }

  Widget getDeathChart() {
    if (widget.coronaTimeSeriesData != null) {
      return SizedBox(child: TimeSeriesChart(AppColors.red_dark, widget.coronaTimeSeriesData.dailyDeceased), width: 50.0, height: 30.0, );
    } else {
      return SizedBox();
    }
  }
}
