import 'dart:convert';

import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/screen/countrydata/custom_expansion_tile.dart';
import 'package:corona_yudiz/screen/countrydata/overview_country.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/screen/home/model/state_model.dart';
import 'package:corona_yudiz/util/date_time_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class CountryDataPage extends BaseScreen {
  @override
  State<CountryDataPage> createState() => _CountryDataState();
}

class _CountryDataState extends BaseScreenState<CountryDataPage>
    with AutomaticKeepAliveClientMixin {
  Dimens _dimens;

  @override
  bool get wantKeepAlive => true;

  bool isDataLoading = true;

  final stateList = List<StateCoronaModel>();
  CoronaTimeSeriesModel coronaTimeSeriesModel;
  CoronaCasesModel totalCasesInIndia;
  final GlobalKey _keyOverView = GlobalKey();

  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    getIndianCoronaCases().then((snapData) {
      _refreshController.refreshCompleted();
      if (mounted) {
        setState(() {
          if (snapData.key != null) {
            stateList.clear();
            stateList
                .addAll(snapData.key.where((item) => item.districts != null));
          }
          isDataLoading = false;

          if (snapData.value != null) {
            coronaTimeSeriesModel = snapData.value;
          }
        });
      }
    });
  }

  @override
  void initState() {
    _onRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    return Container(
      padding: EdgeInsets.all(20.0),
      child: SmartRefresher(
        child: ListView(
          physics: BouncingScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            OverviewCountryWidget(_keyOverView, totalCasesInIndia,
                coronaTimeSeriesData: coronaTimeSeriesModel),
            getStateListContainer(),
          ],
        ),
        header: WaterDropMaterialHeader(
          backgroundColor: AppColors.colorPrimary,
        ),
        controller: _refreshController,
        onRefresh: _onRefresh,
      ),
    );
  }

  Widget getStateListContainer() {
    return Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.only(top: 20.0),
      decoration: BoxDecoration(
        color: AppColors.app_white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).all_india_data,
            style: Styles.tvStyle(fontSize: 14.0, fontWeight: FontWeight.w100),
          ),
          Row(
            children: <Widget>[
              Text(
                AppLocalizations.of(context).last_updated,
                style: Styles.tvStyle(fontSize: 10.0, fontWeight: FontWeight.w500),
              ),
              SizedBox(width: 2.0,),
              Text(
                totalCasesInIndia?.lastupdatedtime == null ? "-" : DateTimeUtil.timeAgoString(DateTimeUtil.getDateFromString(dateString: totalCasesInIndia?.lastupdatedtime, format: AppConstants.REQ_DATE_FORMAT).millisecondsSinceEpoch),
                style: Styles.tvStyle(fontSize: 10.0, fontWeight: FontWeight.w900),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, bottom: 8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: Text(
                        AppLocalizations.of(context).state_ut.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t7,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w500))),
                Expanded(
                    child: Text(
                        AppLocalizations.of(context).confirmed.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t7,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w500))),
                Expanded(
                    child: Text(
                        AppLocalizations.of(context).active.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t7,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w500))),
                Expanded(
                    child: Text(
                        AppLocalizations.of(context).recovered.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t7,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w500))),
                Expanded(
                    child: Text(
                        AppLocalizations.of(context).death.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t7,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w500))),
              ],
            ),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: getStateListWithProgress(),
          ),
        ],
      ),
    );
  }

  Widget getStateListWithProgress() {
    if (isDataLoading) {
      return ProgressWidget();
    } else {
      return ListView.builder(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount: stateList.length,
        itemBuilder: (context, position) {
          return StateCoronaItem(stateList[position]);
        },
      );
    }
  }

  Future<MapEntry<List<StateCoronaModel>, CoronaTimeSeriesModel>>
      getIndianCoronaCases() async {
    var stateCoronaList = List<StateCoronaModel>();
    var coronaTimeSeriesModel;

    final responseState =
        await http.get('https://api.covid19india.org/data.json');
    if (responseState.statusCode == 200) {
      var data = json.decode(responseState.body);
      print("SateWise corona cases response: $data");
      var filter = Map<String, dynamic>.from(data);
      var stateWiseData = filter["statewise"];
      var dayWiseData = filter["cases_time_series"];

      /**
       * get total cases
       */
      var totalCoronaCases = stateWiseData[0];
      totalCasesInIndia = CoronaCasesModel(
          totalCoronaCases["state"],
          int.parse(totalCoronaCases["confirmed"]),
          0,
          int.parse(totalCoronaCases["deaths"]),
          0,
          int.parse(totalCoronaCases["recovered"]),
          int.parse(totalCoronaCases["active"]),
          0);
      totalCasesInIndia.lastupdatedtime = totalCoronaCases["lastupdatedtime"];

      /**
       * get district wise cases
       */
      for (var i = 1; i < stateWiseData.length - 1; i++) {
        stateCoronaList.add(StateCoronaModel(
            stateWiseData[i]["state"],
            int.parse(stateWiseData[i]["confirmed"]),
            int.parse(stateWiseData[i]["recovered"]),
            int.parse(stateWiseData[i]["active"]),
            int.parse(stateWiseData[i]["deaths"]),
            int.parse(stateWiseData[i]["deltaconfirmed"])));
      }

      stateCoronaList.sort((a, b) => b.confirmed.compareTo(a.confirmed));

      /**
       * get day wise cases
       */
      List<String> date = new List<String>();
      List<int> dailyConfirmed = List<int>();
      List<int> dailyDeceased = List<int>();
      List<int> dailyRecovered = List<int>();

      (dayWiseData as List<dynamic>).forEach((item) {
        date.add(item["date"]);
        dailyConfirmed.add(int.parse(item["dailyconfirmed"]));
        dailyDeceased.add(int.parse(item["dailydeceased"]));
        dailyRecovered.add(int.parse(item["dailyrecovered"]));
      });

      coronaTimeSeriesModel = CoronaTimeSeriesModel(
          date, dailyConfirmed, dailyDeceased, dailyRecovered);
    } else {
      throw Exception('Failed to load');
    }

    final responseDistrict =
        await http.get('https://api.covid19india.org/state_district_wise.json');

    if (responseDistrict.statusCode == 200) {
      var filter = json.decode(responseDistrict.body);
//      print("DistrictWise corona cases response: $data");
//      var filter = Map<String, dynamic>.from(data);

      for (var i = 0; i < filter.length - 1; i++) {
        var stateName = filter.keys.elementAt(i);
        for (var k = 0; k < stateCoronaList.length; k++) {
          if (stateCoronaList[k].stateName == stateName) {
            var districtList = List<DistrictCoronaModel>();
            var base1 = filter[stateName];
            var base2 = base1[base1.keys.elementAt(0)];
            for (var j = 0; j < base2.length - 1; j++) {
              var districtName = base2.keys.elementAt(j);
              var cityData = base2[districtName];
              var confirmedCase = cityData["confirmed"];
              districtList
                  .add(DistrictCoronaModel(districtName, confirmedCase));
            }
            stateCoronaList[k].districts = districtList;
          } else {
//            print("State Not Found at " + k.toString());
          }
        }
      }
    } else {
      throw Exception('Failed to load');
    }
    return MapEntry(stateCoronaList, coronaTimeSeriesModel);
  }
}

class StateCoronaItem extends StatefulWidget {
  final StateCoronaModel entry;

  StateCoronaItem(this.entry);

  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);

  @override
  _StateCoronaItemState createState() => _StateCoronaItemState();
}

class _StateCoronaItemState extends State<StateCoronaItem> {
  Dimens _dimens;

  @override
  Widget build(BuildContext context) {
    return _buildTiles(widget.entry);
  }

  Widget _buildTiles(StateCoronaModel root) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    return CustomExpansionTile(
      key: PageStorageKey<StateCoronaModel>(root),
      dataModel: root,
      backgroundColor: Colors.black12,
      children: root.districts.map(_buildExpandedTiles).toList(),
      onExpansionChanged: (bool value) {},
    );
  }

  Widget _buildExpandedTiles(DistrictCoronaModel root) {
    return expandedListItem(root);
  }

  Widget expandedListItem(DistrictCoronaModel coronaModel) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        width: _dimens.w190,
        padding: const EdgeInsets.all(2.0),
        child: Row(
          children: <Widget>[
            Expanded(
                child: Text(
              coronaModel.districtName.toUpperCase(),
              style: Styles.tvStyle(
                  fontSize: _dimens.t12,
                  textColor: AppColors.textColorDarkBlue,
                  fontWeight: FontWeight.w900),
            )),
            Text(
              widget.formatter.format(coronaModel.confirmed).toUpperCase(),
              textAlign: TextAlign.end,
              style: Styles.tvStyle(
                  fontSize: _dimens.t12,
                  textColor: AppColors.textColorDarkBlue,
                  fontWeight: FontWeight.w900),
            ),
          ],
        ),
      ),
    );
  }
}
