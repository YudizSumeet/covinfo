// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/home/model/state_model.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/strings.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';

const Duration _kExpand = Duration(milliseconds: 200);

/// A single-line [ListTile] with a trailing button that expands or collapses
/// the tile to reveal or hide the [children].
///
/// This widget is typically used with [ListView] to create an
/// "expand / collapse" list entry. When used with scrolling widgets like
/// [ListView], a unique [PageStorageKey] must be specified to enable the
/// [ExpansionTile] to save and restore its expanded state when it is scrolled
/// in and out of view.
///
/// See also:
///
///  * [ListTile], useful for creating expansion tile [children] when the
///    expansion tile represents a sublist.
///  * The "Expand/collapse" section of
///    <https://material.io/guidelines/components/lists-controls.html>.
class CustomExpansionTile extends StatefulWidget {
  /// Creates a single-line [ListTile] with a trailing button that expands or collapses
  /// the tile to reveal or hide the [children]. The [initiallyExpanded] property must
  /// be non-null.
  const CustomExpansionTile({
    Key key,
    this.leading,
    @required this.dataModel,
    this.subtitle,
    this.backgroundColor,
    this.onExpansionChanged,
    this.children = const <Widget>[],
    this.trailing,
    this.initiallyExpanded = false,
  })  : assert(initiallyExpanded != null),
        super(key: key);

  /// A widget to display before the title.
  ///
  /// Typically a [CircleAvatar] widget.
  final Widget leading;

  /// Additional content displayed below the title.
  ///
  /// Typically a [Text] widget.
  final Widget subtitle;

  /// Called when the tile expands or collapses.
  ///
  /// When the tile starts expanding, this function is called with the value
  /// true. When the tile starts collapsing, this function is called with
  /// the value false.
  final ValueChanged<bool> onExpansionChanged;

  /// The widgets that are displayed when the tile expands.
  ///
  /// Typically [ListTile] widgets.
  final List<Widget> children;

  /// The color to display behind the sublist when expanded.
  final Color backgroundColor;

  /// A widget to display instead of a rotating arrow icon.
  final Widget trailing;

  /// Specifies if the list tile is initially expanded (true) or collapsed (false, the default).
  final bool initiallyExpanded;

  final StateCoronaModel dataModel;

  @override
  _CustomExpansionTileState createState() => _CustomExpansionTileState();
}

class _CustomExpansionTileState extends State<CustomExpansionTile>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeOutTween =
      CurveTween(curve: Curves.easeOut);
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: -0.25);

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();
  final ColorTween _iconColorTween = ColorTween();
  final ColorTween _backgroundColorTween = ColorTween();

  AnimationController _controller;
  Animation<double> _iconTurns;
  Animation<double> _heightFactor;
  Animation<Color> _borderColor;
  Animation<Color> _headerColor;
  Animation<Color> _iconColor;
  Animation<Color> _backgroundColor;

  bool _isExpanded = false;
  final formatter = new NumberFormat(AppConstants.NUMBER_FORMAT);

  Dimens _dimens;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _kExpand, vsync: this);
    _heightFactor = _controller.drive(_easeInTween);
    _iconTurns = _controller.drive(_halfTween.chain(_easeInTween));
    _borderColor = _controller.drive(_borderColorTween.chain(_easeOutTween));
    _headerColor = _controller.drive(_headerColorTween.chain(_easeInTween));
    _iconColor = _controller.drive(_iconColorTween.chain(_easeInTween));
    _backgroundColor =
        _controller.drive(_backgroundColorTween.chain(_easeOutTween));

    _isExpanded =
        PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            // Rebuild without widget.children.
          });
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null)
      widget.onExpansionChanged(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    return Container(
      margin: EdgeInsets.only(top: 4.0, bottom: 4.0),
      decoration: BoxDecoration(
          color: AppColors.grey_light,
          borderRadius: BorderRadius.all(Radius.circular(2.0))),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          InkWell(
            onTap: _handleTap,
            child: Padding(
              padding: EdgeInsets.all(5.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: RotationTransition(
                          turns: _iconTurns,
                          child: Icon(
                            Icons.play_arrow,
                            size: 16.0,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          widget.dataModel.stateName,
                          style: Styles.tvStyle(
                              fontSize: _dimens.t13,
                              textColor: AppColors.textColorDarkBlue,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      Text(
                        "+ ${widget.dataModel.newCases} New Cases",
                        style: Styles.tvStyle(
                            fontStyle: FontStyle.italic,
                            fontSize: _dimens.t10,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w900),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(child: Container()),
                      Expanded(
                          child: Text(
                        formatter.format(widget.dataModel.confirmed),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t13,
                            textColor: AppColors.red_light,
                            fontWeight: FontWeight.w900),
                      )),
                      Expanded(
                          child: Text(
                        formatter.format(widget.dataModel.active),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t13,
                            textColor: AppColors.yellow_dark,
                            fontWeight: FontWeight.w900),
                      )),
                      Expanded(
                          child: Text(
                        formatter.format(widget.dataModel.recovered),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t13,
                            textColor: AppColors.blue_light,
                            fontWeight: FontWeight.w900),
                      )),
                      Expanded(
                          child: Text(
                        formatter.format(widget.dataModel.death),
                        textAlign: TextAlign.center,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t13,
                            textColor: AppColors.red_dark,
                            fontWeight: FontWeight.w900),
                      )),
                    ],
                  ),
                ],
              ),
            ),
          ),
          ClipRect(
            child: Align(
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  Widget _childrenTitle(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
        width: _dimens.w190,
        padding:
            const EdgeInsets.only(left: 2.0, top: 0.0, right: 2.0, bottom: 4.0),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                "DISTRICT",
                style: Styles.tvStyle(
                    fontSize: _dimens.t10,
                    textColor: AppColors.textColorDarkBlue,
                    fontWeight: FontWeight.w600),
              ),
            ),
            Text(
              AppLocalizations.of(context).confirmed.toUpperCase(),
              style: Styles.tvStyle(
                  fontSize: _dimens.t10,
                  textColor: AppColors.textColorDarkBlue,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void didChangeDependencies() {
    final ThemeData theme = Theme.of(context);
    _borderColorTween..end = theme.dividerColor;
    _headerColorTween
      ..begin = theme.textTheme.subhead.color
      ..end = theme.accentColor;
    _iconColorTween
      ..begin = theme.unselectedWidgetColor
      ..end = theme.accentColor;
    _backgroundColorTween..end = widget.backgroundColor;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    final bool closed = !_isExpanded && _controller.isDismissed;
    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed
          ? null
          : Padding(
              padding: EdgeInsets.only(
                  left: 24.0, top: 8.0, right: 24.0, bottom: 16.0),
              child: Column(
                children: <Widget>[
                  _childrenTitle(context),
                  Column(children: widget.children)
                ],
              ),
            ),
    );
  }
}
