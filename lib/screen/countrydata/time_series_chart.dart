import 'package:corona_yudiz/constants.dart';
import 'package:flutter/material.dart';
import 'package:mp_chart/mp/chart/line_chart.dart';
import 'package:mp_chart/mp/controller/line_chart_controller.dart';
import 'package:mp_chart/mp/core/adapter_android_mp.dart';
import 'package:mp_chart/mp/core/data/line_data.dart';
import 'package:mp_chart/mp/core/data_interfaces/i_line_data_set.dart';
import 'package:mp_chart/mp/core/data_set/line_data_set.dart';
import 'package:mp_chart/mp/core/description.dart';
import 'package:mp_chart/mp/core/entry/entry.dart';
import 'package:mp_chart/mp/core/enums/legend_form.dart';
import 'package:mp_chart/mp/core/enums/mode.dart';
import 'package:mp_chart/mp/core/image_loader.dart';
import 'package:mp_chart/mp/core/utils/color_utils.dart';

class TimeSeriesChart extends StatefulWidget {
  final Color color;
  final List<int> dailyCases;

  TimeSeriesChart(this.color, this.dailyCases);

  @override
  State<StatefulWidget> createState() {
    return TimeSeriesChartState();
  }
}

class TimeSeriesChartState extends State<TimeSeriesChart> {
  LineChartController controller;
  static TypeFace EXTRA_BOLD =
      TypeFace(fontFamily: "Roboto", fontWeight: FontWeight.w900);

  @override
  void initState() {
    _initController();
    _initLineData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _initLineChart();
  }

  void _initController() {
    var desc = Description()..enabled = false;

    controller = LineChartController(
        axisLeftSettingFunction: (axisLeft, controller) {
          axisLeft
            ..drawAxisLine = false
            ..drawLabels = false
            ..drawGridLinesBehindData = false
            ..drawLimitLineBehindData = false
            ..drawGridLines = false
            ..setAxisMaximum(200)
            ..setAxisMinimum(-50);
        },
        axisRightSettingFunction: (axisRight, controller) {
          axisRight.enabled = (false);
        },
        legendSettingFunction: (legend, controller) {
          legend.shape = (LegendForm.LINE);
        },
        xAxisSettingFunction: (xAxis, controller) {
          xAxis
            ..drawAxisLine = false
            ..drawLabels = false
            ..drawGridLinesBehindData = false
            ..drawLimitLineBehindData = false
            ..drawGridLines = false;
        },
        drawGridBackground: false,
        backgroundColor: ColorUtils.WHITE,
        dragXEnabled: false,
        dragYEnabled: false,
        scaleXEnabled: false,
        scaleYEnabled: false,
        minOffset: 1.0,
        extraBottomOffset: -20.0,
        pinchZoomEnabled: false,
        drawMarkers: false,
        drawBorders: false,
        description: desc);
  }

  void _initLineData() async {
    print("list Size: ${widget.dailyCases.length}");
    List<Entry> values = List();
//    var img = await ImageLoader.loadImage('${AppConstants.localImageBasePath}ic_about_us.png');
    var lastIndex = widget.dailyCases.length;
    for (int index = 0; index < lastIndex; index++) {
      double val = widget.dailyCases[index].toDouble();
      values.add(Entry(x: index.toDouble(), y: val));
    }
//    values.add(Entry(x: lastIndex.toDouble() , y: (widget.dailyCases[lastIndex]).toDouble(), icon: img));

    LineDataSet set1;

    set1 = LineDataSet(values, null);

    set1.setDrawIcons(true);
    set1.setFillAlpha(0);
    set1.setDrawCircles(false);
    set1.setDrawHighlightIndicators(false);
    set1.setDrawValues(false);

    set1.setColor1(widget.color);
    set1.setLineWidth(2.0);
    set1.setForm(null);
    set1.setDrawFilled(true);

    List<ILineDataSet> dataSets = List();
    dataSets.add(set1);
    controller.data = LineData.fromList(dataSets);
    setState(() {});
  }

  Widget _initLineChart() {
    var lineChart = LineChart(controller);
    controller.animator
      ..reset()
      ..animateX1(1500);
    return lineChart;
  }
}
