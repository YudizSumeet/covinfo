import 'dart:convert';

import 'package:corona_yudiz/api/api_util.dart';
import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/base/base_screen_api.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/scaffold.dart';
import 'package:corona_yudiz/custom/toolbar.dart';
import 'package:corona_yudiz/screen/source/source_model.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class SourcePage extends BaseApiScreen {
  @override
  State<SourcePage> createState() => _SourcePageState();
}

class _SourcePageState extends BaseScreenState<SourcePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  SourceModel model;
  List<String> sources = List();

  @override
  void initState() {
    model = SourceModel();
    getSources();
    model.sourceState.observe((value) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      toolbar: Toolbar(
        showBackIcon: true,
        title: "Sources",
        onPressed: () {},
      ),
      frame: Container(
          margin: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
              color: AppColors.app_white,
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: ListView.separated(
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(15),
              itemBuilder: (_, pos) {
                return GestureDetector(
                  onTap: () {
                    AppUtil.openBrowser(sources[pos]);
                  },
                  child: Text(
                    "${(pos + 1)}. ${sources[pos]}",
                    style: Styles.tvStyle(
                        fontSize: 15.0, fontWeight: FontWeight.w500),
                  ),
                );
              },
              separatorBuilder: (_, __) {
                return SizedBox(
                  height: 5,
                );
              },
              itemCount: sources.length)),
    );
  }

  void getSources() async {
    Response res = await get(APIEndPoint.SOURCES);
    List<dynamic> body = jsonDecode(res.body);
    List<String> posts = body.map((s) => s as String).toList();
    String sources = "";
    for (var pos = 0; pos < posts.length; pos++) {
      sources = sources + (pos + 1).toString() + ". " + posts[pos] + "\n";
    }

    setState(() {
      this.sources = posts;
    });
  }
}
