import 'package:corona_yudiz/base/base_model.dart';
import 'package:corona_yudiz/screen/entry/main_repo.dart';
import 'package:corona_yudiz/screen/source/source_repo.dart';
import 'package:corona_yudiz/util/obervable.dart';

class SourceModel extends BaseModel {
  Observable<String> sourceState = Observable();
  SourceRepo repo = SourceRepo();

  void sources() async {
    sourceState.setValue((await repo.sources(onApiError))?.toString());
  }
}
