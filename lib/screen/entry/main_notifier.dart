import 'package:flutter/cupertino.dart';

class MainNotifier extends ChangeNotifier {
  int _currentTabPos = 0;

  int get currentTabPos {
    return _currentTabPos;
  }

  void onTabChanged(int newPos) {
    _currentTabPos = newPos;
    notifyListeners();
  }
}
