import 'package:corona_yudiz/api/api_error.dart';
import 'package:corona_yudiz/base/base_repo.dart';
import 'package:corona_yudiz/util/obervable.dart';

class MainRepo extends BaseRepo {
  Observable<String> loginState = Observable();

  dynamic login(Observable<ApiError> onApiError) async {
    return await callApi(() {
      return apiService.login();
    }, onApiError, reqCode: 230);
  }

}
