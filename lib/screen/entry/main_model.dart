import 'package:corona_yudiz/base/base_model.dart';
import 'package:corona_yudiz/screen/entry/main_repo.dart';
import 'package:corona_yudiz/util/obervable.dart';

class MainModel extends BaseModel {
  Observable<String> loginState = Observable();
  MainRepo repo = MainRepo();

  void login() async {
    loginState.setValue((await repo.login(onApiError))?.toString());
  }

}
