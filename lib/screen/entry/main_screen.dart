import 'dart:async';

import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/custom/scaffold.dart';
import 'package:corona_yudiz/custom/toolbar.dart';
import 'package:corona_yudiz/data/local/tab_model.dart';
import 'package:corona_yudiz/screen/countrydata/country_data.dart';
import 'package:corona_yudiz/screen/countrypicker/country_list.dart';
import 'package:corona_yudiz/screen/entry/main_notifier.dart';
import 'package:corona_yudiz/screen/home/home.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/screen/map/covid_map.dart';
import 'package:corona_yudiz/screen/more/more.dart';
import 'package:corona_yudiz/screen/resources/resources.dart';
import 'package:corona_yudiz/screen/source/source.dart';
import 'package:corona_yudiz/util/app_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainScreen extends BaseScreen {
  final CountryListModel selCountry;
  static Map snapData;

  MainScreen(this.selCountry);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends BaseScreenState<MainScreen>
    with TickerProviderStateMixin {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  CoronaCasesModel allOverCoronaCases;
  List<CoronaCasesModel> countryWiseCoronaCases = List();

  TabController _tabController;

  MainNotifier _notifier;
  SharedPreferences prefs;

  iniPrefs() async {
    prefs = await getPrefs();
    print("coronal level key init pref  $prefs");
  }

  void onDataChange(CoronaCasesModel allOverCoronaCases,
      List<CoronaCasesModel> countryWiseCoronaCases) {
    setState(() {
      this.allOverCoronaCases = allOverCoronaCases;
      this.countryWiseCoronaCases.clear();
      this.countryWiseCoronaCases.addAll(countryWiseCoronaCases);
    });
  }

  @override
  Future<void> initState() {
    iniPrefs();
    var localNotification = FlutterLocalNotificationsPlugin()
      ..initialize(InitializationSettings(
          AndroidInitializationSettings('ic_icon'),
          IOSInitializationSettings()));
    var localNotificationSettings = NotificationDetails(
        AndroidNotificationDetails('Covinfo', 'Covinfo', 'Covinfo',
            importance: Importance.Max, priority: Priority.High),
        IOSNotificationDetails());

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        await localNotification.show(0, message["notification"]["title"],
            message["notification"]["body"], localNotificationSettings);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );

    _notifier = MainNotifier();
    _tabController = TabController(length: getTabList().length, vsync: this);

    _tabController.addListener(() {
      if (_tabController.indexIsChanging)
        _notifier.onTabChanged(_tabController.index);
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _checkLatestVersion();
    });

    /**
     * default select corona cases in india day wise chart
     */
    if (!isIndiaSelected()) {
      HomePage.countryOfNoOfCases = widget.selCountry;
    } else {
      HomePage.countryOfNoOfCases = null;
    }
    super.initState();
  }

  void checkCoronaLevel() {
//    var ref = prefs.getBool(AppConstants.KEY_CORONA_LEVEL) ?? false;
    if (!prefs.containsKey(AppConstants.KEY_CORONA_LEVEL))
      _showShowcaseView(context);
  }

  _checkLatestVersion() async {
    String currentVersion = await AppUtil.getVersion();
    final RemoteConfig remoteConfig = await RemoteConfig.instance;

    final defaults = <String, dynamic>{'version': currentVersion};
    await remoteConfig.setDefaults(defaults);

    await remoteConfig.fetch(expiration: const Duration(seconds: 10));
    await remoteConfig.activateFetched();

    if (currentVersion != remoteConfig.getString('version'))
      showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Update required!",
                style: Styles.tvStyle(
                    fontWeight: FontWeight.bold, fontSize: 20.0)),
            content: Text("Please update your app to the latest version.",
                style: Styles.tvStyle(fontSize: 15.0)),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancel',
                    style: Styles.tvStyle(
                        fontWeight: FontWeight.bold, textColor: Colors.red)),
                onPressed: () {
                  SystemChannels.platform
                      .invokeMethod<void>('SystemNavigator.pop');
                },
              ),
              FlatButton(
                child: Text('Update',
                    style: Styles.tvStyle(
                        fontWeight: FontWeight.bold,
                        textColor: AppColors.colorPrimary)),
                onPressed: () {
                  AppUtil.openBrowser("https://www.covinfo.org/");
                },
              ),
            ],
          );
        },
      );
    else {
      //corona level popup
      checkCoronaLevel();
    }
  }

  void _showShowcaseView(BuildContext context) async {
    await Future.delayed(Duration(seconds: 1));

    prefs.setBool(AppConstants.KEY_CORONA_LEVEL, true);
    print(
        "coronal level key1  ${prefs.getBool(AppConstants.KEY_CORONA_LEVEL)}");

    OverlayState _showcaseState = Overlay.of(context);
    OverlayEntry _showcaseEntry = OverlayEntry(
        builder: (_) => Scaffold(
              backgroundColor: Color(0xd6101853),
              body: SafeArea(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 15, right: 15),
                        height: 30,
                        width: 30,
                        child: Image.asset(
                          "${AppConstants.localImageBasePath}ic_chat_bot.png",
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20, right: 10),
                        width: 70,
                        child: Image.asset(
                            "${AppConstants.localImageBasePath}ic_arrow.png"),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10, right: 40),
                        child: Text(
                          "Corona virus\nrisk scan",
                          style: Styles.tvStyle(
                              textColor: Colors.white,
                              fontSize: 25.0,
                              fontWeight: FontWeight.w900,
                              fontFamily: "SFUIText"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ));
    _showcaseState.insert(_showcaseEntry);

    await Future.delayed(Duration(seconds: 3));

    _showcaseEntry.remove();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        SystemChannels.platform
            .invokeMethod<void>('SystemNavigator.pop');
        return true;
      },
      child: ChangeNotifierProvider(
        create: (_) => _notifier,
        child: CustomScaffold(
          toolbar: Consumer<MainNotifier>(
            builder: (_, notifier, __) => Toolbar(
              onPressed: (){},
              title: getTabList()[notifier.currentTabPos].title,
              rightIconName:
                  "${AppConstants.localImageBasePath}ic_chat_bot.png",
            ),
          ),
          frame: Column(
            children: <Widget>[
              Expanded(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: getPages()),
              ),
              Consumer<MainNotifier>(
                builder: (_, notifier, __) => Theme(
                  data: ThemeData(
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent),
                  child: Container(
                    color: Color(0xff0E1256),
                    child: TabBar(
                        controller: _tabController,
                        indicatorColor: Colors.transparent,
                        tabs: [
                          for (int index = 0;
                              index < getTabList().length;
                              index++)
                            _getTab(index, notifier.currentTabPos)
                        ]),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  List<TabModel> getTabList() {
    if (isIndiaSelected()) {
      return [
        TabModel("COUNTRY DATA", "ic_home.png"),
        TabModel("COVINFO", "ic_statistics.png"),
        TabModel("WORLD DATA MAP", "ic_map.png"),
        TabModel("RESOURCES", "ic_resources.png"),
        TabModel("MORE", "ic_menu.png"),
      ];
    } else {
      return [
        TabModel("COVINFO", "ic_statistics.png"),
        TabModel("WORLD DATA MAP", "ic_map.png"),
        TabModel("RESOURCES", "ic_resources.png"),
        TabModel("MORE", "ic_menu.png"),
      ];
    }
  }

  List<Widget> getPages() {
    if (isIndiaSelected()) {
      return [
        CountryDataPage(),
        HomePage(
            allOverCoronaCases: allOverCoronaCases,
            countryWiseCoronaCases: countryWiseCoronaCases,
            onDataChange: onDataChange),
        ActCovidMap(
            allOverCoronaCases: allOverCoronaCases,
            countryWiseCoronaCases: countryWiseCoronaCases),
        ResourcesPage(),
        MorePage()
      ];
    } else {
      return [
        HomePage(
            allOverCoronaCases: allOverCoronaCases,
            countryWiseCoronaCases: countryWiseCoronaCases,
            onDataChange: onDataChange),
        ActCovidMap(
            allOverCoronaCases: allOverCoronaCases,
            countryWiseCoronaCases: countryWiseCoronaCases),
        ResourcesPage(),
        MorePage()
      ];
    }
  }

  Tab _getTab(int pos, int currentPos) {
    String icon = getTabList()[pos].icon;
    if (pos == currentPos) icon = icon.replaceFirst(".png", "_selected.png");

    return Tab(
      icon: SizedBox(
        height: 25,
        width: 25,
        child: Image.asset(AppConstants.localImageBasePath + icon),
      ),
    );
  }

  bool isIndiaSelected() {
    return widget.selCountry != null &&
        widget.selCountry.alpha2Code == AppConstants.INDIA_COUNTRY_CODE;
  }
}
