import 'dart:convert';

import 'package:corona_yudiz/api/api_error.dart';
import 'package:corona_yudiz/base/base_screen_api.dart';
import 'package:corona_yudiz/constants.dart';
import 'package:corona_yudiz/screen/countrypicker/country_list.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/screen/home/model/corona_cases_model.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/util/obervable.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends BaseApiScreen {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends BaseApiScreenState<SplashScreen> {
   SharedPreferences prefs;
   CountryListModel selCountry;

  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIOverlays([]);
    initPrefs();
    Future.delayed(Duration(seconds: 3), () {
      if (prefs.containsKey(AppConstants.KEY_SEL_COUNTRY) && prefs.getString(AppConstants.KEY_SEL_COUNTRY)!=null)
        GetIt.I<NavigationUtil>().navigatePushReplace(MainScreen(selCountry));
      else {
        GetIt.I<NavigationUtil>().navigatePushReplace(CountryListPage(fromSplash: true));
      }
    });
  }

   initPrefs() async {
     prefs = await getPrefs();
     String modelCountry = (prefs.getString(AppConstants.KEY_SEL_COUNTRY));
     if (modelCountry != null) {
       var parsedJson = json.decode(modelCountry);
       selCountry = CountryListModel.fromJson(parsedJson);
     }
   }

   @override
  void dispose() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.colorPrimary,
      body: Center(
        child: SizedBox(
            height: 200,
            width: 200,
            child:
            Image.asset(AppConstants.localImageBasePath + "ic_logo_covinfo.png")),
      ),
    );
  }

  @override
  Observable<ApiError> onApiError() {
    return null;
  }
}
