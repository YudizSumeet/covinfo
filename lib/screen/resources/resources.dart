import 'dart:collection';
import 'dart:convert';

import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/screen/resources/resource_list.dart';
import 'package:corona_yudiz/screen/resources/resources_data.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'dart:io';

class ResourcesPage extends BaseScreen {
  @override
  _ResourcesPageState createState() => _ResourcesPageState();
}

class _ResourcesPageState extends BaseScreenState<ResourcesPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  static Future<Response> resourcesData;
  static PageStorageKey listKey;

  Dimens _dimens;

  @override
  void initState() {
    if (resourcesData == null) {
      resourcesData = getData();
      listKey = PageStorageKey("a");
    }
    super.initState();
  }

  Future<Response> getData() {
    return get("https://api.covid19india.org/resources/resources.json");
  }

  @override
  Widget build(BuildContext context) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    return Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
            color: AppColors.app_white,
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        child: FutureBuilder(
          future: resourcesData,
          builder: (_, snapshot) {
            if (snapshot.data != null) {
              var resources = _processResources(snapshot.data);
              var categories = resources.keys.toList();
              return ListView.separated(
                key: listKey,
                padding: const EdgeInsets.all(15),
                physics: BouncingScrollPhysics(),
                itemCount: categories.length,
                itemBuilder: (_, pos) {
                  var category = categories[pos];
                  return GestureDetector(
                    onTap: () {
                      GetIt.I<NavigationUtil>()
                          .navigatePush(ResourceList(resources[category]));
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: AppColors.grey_light,
                          borderRadius: BorderRadius.all(Radius.circular(2.0))),
                      child: Text(
                        category,
                        style: Styles.tvStyle(
                            fontSize: _dimens.t13,
                            textColor: AppColors.textColorDarkBlue,
                            fontWeight: FontWeight.w900),
                      ),
                    ),
                  );
                },
                separatorBuilder: (_, __) {
                  return SizedBox(
                    height: 10,
                  );
                },
              );
            } else
              return ProgressWidget();
          },
        ));
  }

  LinkedHashMap<String, List<Resource>> _processResources(Response resources) {
    LinkedHashMap<String, List<Resource>> resourceMap = LinkedHashMap();

    var resourceList =
        ResourcesModel.fromJson(json.decode(resources.body)).resources;

    [
      "CoVID-19 Testing Lab",
      "Free Food",
      "Accommodation and Shelter Homes",
      "Community Kitchen",
      "Delivery [Vegetables, Fruits, Groceries, Medicines, etc.]",
      "Hospitals and Centers",
      "Government Helpline",
      "Police",
      "Fundraisers",
      "Mental well being and Emotional support",
      "Senior Citizen Support",
      "Ambulance",
      "Transportation",
      "Fire services",
      "Other"
    ].forEach((category) {
      resourceMap[category] = resourceList.where((resource) {
        return resource.category == category;
      }).toList();
    });

    return resourceMap;
  }
}
