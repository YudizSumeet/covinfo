// To parse this JSON data, do
//
//     final resourcesModel = resourcesModelFromJson(jsonString);

import 'dart:convert';

ResourcesModel resourcesModelFromJson(String str) => ResourcesModel.fromJson(json.decode(str));

String resourcesModelToJson(ResourcesModel data) => json.encode(data.toJson());

class ResourcesModel {
    List<Resource> resources;

    ResourcesModel({
        this.resources,
    });

    factory ResourcesModel.fromJson(Map<String, dynamic> json) => ResourcesModel(
        resources: List<Resource>.from(json["resources"].map((x) => Resource.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "resources": List<dynamic>.from(resources.map((x) => x.toJson())),
    };
}

class Resource {
    String category;
    String city;
    String contact;
    String descriptionandorserviceprovided;
    String nameoftheorganisation;
    String phonenumber;
    String state;

    Resource({
        this.category,
        this.city,
        this.contact,
        this.descriptionandorserviceprovided,
        this.nameoftheorganisation,
        this.phonenumber,
        this.state,
    });

    factory Resource.fromJson(Map<String, dynamic> json) => Resource(
        category: json["category"],
        city: json["city"],
        contact: json["contact"],
        descriptionandorserviceprovided: json["descriptionandorserviceprovided"],
        nameoftheorganisation: json["nameoftheorganisation"],
        phonenumber: json["phonenumber"],
        state: json["state"],
    );

    Map<String, dynamic> toJson() => {
        "category": category,
        "city": city,
        "contact": contact,
        "descriptionandorserviceprovided": descriptionandorserviceprovided,
        "nameoftheorganisation": nameoftheorganisation,
        "phonenumber": phonenumber,
        "state": state,
    };
}
