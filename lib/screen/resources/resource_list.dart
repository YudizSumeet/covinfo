import 'package:corona_yudiz/base/base_screen.dart';
import 'package:corona_yudiz/base/base_screen_api.dart';
import 'package:corona_yudiz/custom/scaffold.dart';
import 'package:corona_yudiz/custom/toolbar.dart';
import 'package:corona_yudiz/screen/resources/resources_data.dart';
import 'package:corona_yudiz/values/colors.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/styles.dart';
import 'package:flutter/material.dart';

class ResourceList extends BaseApiScreen {
  List<Resource> resources;

  ResourceList(this.resources);

  @override
  State<ResourceList> createState() => _ResourceListState();
}

class _ResourceListState extends BaseScreenState<ResourceList> {
  Dimens _dimens;
  @override
  Widget build(BuildContext context) {
    _dimens = Dimens(size: MediaQuery.of(context).size);

    return CustomScaffold(
      toolbar: Toolbar(
        showBackIcon: true,
        title: "RESOURCES",
        onPressed: () {},
      ),
      frame: Container(
          margin: EdgeInsets.all(20.0),
          decoration: BoxDecoration(
              color: AppColors.app_white,
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: ListView.separated(
              physics: BouncingScrollPhysics(),
              padding: const EdgeInsets.all(15),
              itemBuilder: (_, pos) {
                var resource = widget.resources[pos];
                return Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: AppColors.grey_light,
                      borderRadius: BorderRadius.all(Radius.circular(2.0))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          resource.nameoftheorganisation,
                          style: Styles.tvStyle(
                              fontSize: _dimens.t15,
                              textColor: AppColors.textColorDarkBlue,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Flexible(
                        child: Text(
                          "${resource.city}, ${resource.state}",
                          style: Styles.tvStyle(
                              fontSize: _dimens.t13,
                              textColor: AppColors.textColorDarkBlue,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Flexible(
                        child: Text(
                          resource.descriptionandorserviceprovided,
                          style: Styles.tvStyle(
                              fontSize: _dimens.t13,
                              textColor: AppColors.grey_hint,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Flexible(
                        child: Text(
                          "Phone : ${resource.phonenumber.replaceAll("\n", " ")}",
                          style: Styles.tvStyle(
                              fontSize: _dimens.t13,
                              textColor: AppColors.textColorDarkBlue,
                              fontWeight: FontWeight.w900),
                        ),
                      ),
                    ],
                  ),
                );
              },
              separatorBuilder: (_, __) {
                return SizedBox(
                  height: 5,
                );
              },
              itemCount: widget.resources.length)),
    );
  }
}
