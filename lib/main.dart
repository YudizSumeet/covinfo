import 'package:corona_yudiz/screen/entry/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:corona_yudiz/api/api_util.dart';
import 'package:corona_yudiz/screen/entry/main_screen.dart';
import 'package:corona_yudiz/util/navigation_util.dart';
import 'package:corona_yudiz/values/dimens.dart';
import 'package:corona_yudiz/values/strings.dart';

import 'app_notifier.dart';
import 'base/base_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  var getIt = GetIt.instance;

  getIt.registerLazySingleton(() => NavigationUtil());
  getIt.registerLazySingleton(() => ApiUtil());

  runApp(App());
}

class App extends BaseScreen {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends BaseScreenState<App> {
  @override
  void initState() {
    super.initState();

//    GetIt.I
//        .registerLazySingleton(() => Dimens(size: MediaQuery.of(context).size));
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AppNotifier()),
      ],
      child: Consumer<AppNotifier>(
        builder: (_, notifier, __) {
          return MaterialApp(
            home: SplashScreen(),
            debugShowCheckedModeBanner: false,
            navigatorKey: GetIt.I<NavigationUtil>().navigatorKey,
            locale: notifier.currentLocale,
            supportedLocales: [const Locale("en")],
            localizationsDelegates: [
              AppLocalizationsDelegate(),
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
          );
        },
      ),
    );
  }
}
