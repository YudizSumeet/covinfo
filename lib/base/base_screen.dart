import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/util/toast_util.dart';
import 'package:corona_yudiz/util/toast_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// A parent class for all the stateful widgets which do not require API calls

abstract class BaseScreen extends StatefulWidget {

}

abstract class BaseScreenState<T extends BaseScreen> extends State<T> {

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<SharedPreferences> getPrefs(){
    return _prefs;
  }

  void successToast({@required String message}) {
    ToastUtil.successSnackbar(context, message);
  }

  void errorToast({@required String message}) {
    ToastUtil.errorSnackbar(context, message);
  }

  showProgress() {
    Progress.showProgress(context);
  }

  hideProgress() {
    Progress.hideProgress();
  }
}
