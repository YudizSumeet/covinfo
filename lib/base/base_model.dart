import 'package:corona_yudiz/api/api_error.dart';
import 'package:corona_yudiz/util/obervable.dart';

class BaseModel {
  Observable<ApiError> onApiError = Observable();
}
