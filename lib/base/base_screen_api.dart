import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:corona_yudiz/api/api_error.dart';
import 'package:corona_yudiz/api/api_util.dart';
import 'package:corona_yudiz/custom/progress.dart';
import 'package:corona_yudiz/util/obervable.dart';
import 'package:corona_yudiz/util/toast_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'base_screen.dart';

/// A parent class for all the stateful widgets which include API calls

abstract class BaseApiScreen extends BaseScreen {

}

abstract class BaseApiScreenState<T extends BaseApiScreen> extends State<T> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  ApiUtil apiService = GetIt.I<ApiUtil>();

  Observable<ApiError> onApiError();

  @override
  void initState() {
    super.initState();

    onApiError()?.observe((apiError) {
      errorToast(message: apiError.reqCode.toString());
    });
  }

  Future<SharedPreferences> getPrefs(){
    return _prefs;
  }

  void successToast({@required String message}) {
    ToastUtil.successSnackbar(context, message);
  }

  void errorToast({@required String message}) {
    ToastUtil.errorSnackbar(context, message);
  }

  showProgress() {
    Progress.showProgress(context);
  }

  hideProgress() {
    Progress.hideProgress();
  }

  @override
  void dispose() {
    Progress.hideProgress();
    apiService.cancelRequests();

    super.dispose();
  }
}
