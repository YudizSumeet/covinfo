import 'package:get_it/get_it.dart';
import 'package:corona_yudiz/api/api_error.dart';
import 'package:corona_yudiz/api/api_util.dart';
import 'package:corona_yudiz/util/obervable.dart';

class BaseRepo {
  final ApiUtil apiService = GetIt.I<ApiUtil>();

  dynamic callApi(
      Future<dynamic> Function() apiBlock, Observable<ApiError> onApiError,
      {int reqCode = -1}) async {
    return await apiBlock().catchError((error) {
      onApiError.setValue(ApiError(error, reqCode));
      return null;
    });
  }
}
