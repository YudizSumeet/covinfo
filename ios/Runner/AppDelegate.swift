import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
  GMSServices.provideAPIKey("AIzaSyDKoxPLu2E_l00SA5RQIp-6EGngoFmbvkg")
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
